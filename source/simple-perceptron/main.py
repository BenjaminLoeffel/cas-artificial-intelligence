import logging
from typing import List

import matplotlib.pyplot as plt

from app.dataset_generator import DatasetGenerator
from app.experiment import Experiment
from app.prediction import Prediction
from app.result import Result
from app.simple_perceptron import SimplePerceptron
from app.visualizer import DatasetVisualizer

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger('matplotlib').setLevel(logging.ERROR)


def main():
    try:

        # Generate different data
        # Generate different model behaviour for decision boundary

        # Experiment 1
        # generate less data to train from

        # Experiment 2
        # generate more data to train from

        # Assess
        # precision with new but same testdata set

        # Experiment 3
        # Create variation of learning rate

        # Assess
        # What's the impact of the learning rate

        logging.info("main started")

        experiments = [Experiment(n_samples=100, learning_rate=0.01, epochs=100),
                       Experiment(n_samples=1000, learning_rate=0.01, epochs=100),
                       Experiment(n_samples=10000, learning_rate=0.01, epochs=100)]

        results: List[Result] = []

        for experiment in experiments:
            logging.info(f"running next experiment")
            dataset_generator = DatasetGenerator(n_samples=experiment.n_samples)

            dataset = dataset_generator.create_dataset()

            DatasetVisualizer.visualize(dataset=dataset)

            simple_perceptron = SimplePerceptron(dim_inputs=dataset.features.shape[1],
                                                 epochs=experiment.epochs,
                                                 learning_rate=experiment.learning_rate)

            simple_perceptron.train(training_inputs=dataset.features, labels=dataset.labels)

            predicted_labels = simple_perceptron.predict_batch(dataset.features)

            prediction = Prediction(predicted_label=predicted_labels,
                                    actual_label=dataset.labels)

            result = Result(experiment=experiment,
                            dataset=dataset,
                            prediction=prediction)

            logging.info(
                f"result with n_samples={experiment.n_samples}, learning_rate={experiment.learning_rate} has a accuracy of {prediction.get_accuracy()}")
            results.append(result)

        plt.show()

    except Exception as e:
        logging.exception("exception occurred", exc_info=True)

    finally:
        logging.info("main finished")


if __name__ == "__main__":
    main()
