from app.dataset import Dataset
from app.experiment import Experiment
from app.prediction import Prediction


class Result:
    experiment: Experiment
    dataset: Dataset
    prediction: Prediction

    def __init__(self, experiment: Experiment, dataset: Dataset, prediction: Prediction):
        self.experiment = experiment
        self.dataset = dataset
        self.prediction = prediction
