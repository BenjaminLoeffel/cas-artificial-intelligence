class Experiment:
    n_samples: int
    learning_rate: float
    epochs: int

    def __init__(self, n_samples: int, learning_rate: float, epochs: int):
        self.n_samples = n_samples
        self.learning_rate = learning_rate
        self.epochs = epochs
