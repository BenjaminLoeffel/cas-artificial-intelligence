import logging

import numpy as np


class SimplePerceptron:
    _dim_inputs: int
    _epochs: int
    _learning_rate: float

    def __init__(self, dim_inputs: int, epochs: int = 100, learning_rate: float = 0.01):
        self._dim_inputs = dim_inputs
        self._epochs = epochs
        self._learning_rate = learning_rate
        self.weights = np.zeros(dim_inputs + 1)  # plus 1 for bias

    def predict_batch(self, inputs: np.ndarray) -> np.ndarray:
        res_vector = np.dot(inputs, self.weights[1:]) + self.weights[0]
        activations = [1 if elem > 0 else 0 for elem in res_vector]
        return np.array(activations)

    def predict(self, inputs: np.ndarray) -> int:
        res = np.dot(inputs, self.weights[1:]) + self.weights[0]
        # self.weights[0] is the bias
        if res > 0:
            activation = 1
        else:
            activation = 0
        return activation

    def train(self, training_inputs: np.ndarray, labels: np.ndarray):
        logging.info("start training")
        for epoch in range(self._epochs):
            for inputs, label in zip(training_inputs, labels):
                prediction = self.predict(inputs)
                self.weights[1:] += self._learning_rate * (label - prediction) * inputs  # update weights
                self.weights[0] += self._learning_rate * (label - prediction)  # update bias
            logging.debug(f"epoch: {epoch}")
        logging.info("finished training")
