import random
from typing import Tuple

import numpy as np

from app.dataset import Dataset


class DatasetGenerator:
    n_samples: int
    x: np.ndarray

    def __init__(self, n_samples: int):
        self.n_samples = n_samples
        self.x = np.linspace(0, 10, n_samples)

    @staticmethod
    def _model_function(x, m, b):
        return m * x + b

    @staticmethod
    def _apply_noise(x: np.ndarray, factor: float, mu: float = 2, sigma: float = 4, ):
        return [_x + factor * abs(random.gauss(mu, sigma)) for _x in x]

    def _create_data(self, x: np.ndarray, m: float, b: float, label: int) -> Tuple[np.ndarray, np.ndarray]:
        x_values = x
        y_values = self._model_function(x=x_values, m=m, b=b)
        return (np.column_stack((x_values, y_values)), np.zeros(len(x)) + label)

    def create_dataset(self) -> Dataset:
        features_class_zero, labels_class_zero = self._create_data(x=self.x, m=10, b=5, label=0)
        features_class_zero = self._apply_noise(x=features_class_zero, factor=1)

        features_class_one, labels_class_one = self._create_data(x=self.x, m=10, b=5, label=1)
        features_class_one = self._apply_noise(x=features_class_one, factor=-1)

        features = np.vstack((features_class_zero, features_class_one))
        labels = np.hstack((labels_class_zero, labels_class_one)).T
        return Dataset(features=features, labels=labels)
