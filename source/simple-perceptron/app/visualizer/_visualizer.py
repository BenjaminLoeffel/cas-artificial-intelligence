import matplotlib.pyplot as plt

from app.dataset import Dataset


class DatasetVisualizer:

    @staticmethod
    def visualize(dataset: Dataset):
        plt.figure()
        plt.scatter(dataset.features[:, 0], dataset.features[:, 1], c=dataset.labels, )
        plt.legend()
