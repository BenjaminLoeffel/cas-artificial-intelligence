import numpy as np


class Dataset:
    features: np.ndarray
    labels: np.ndarray

    def __init__(self, features: np.ndarray, labels: np.ndarray):
        self.features = features
        self.labels = labels
