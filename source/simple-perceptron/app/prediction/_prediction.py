import numpy as np
from sklearn.metrics import accuracy_score


class Prediction:
    predicted_label: np.ndarray
    actual_label: np.ndarray

    def __init__(self, predicted_label: np.ndarray, actual_label: np.ndarray):
        self.predicted_label = predicted_label
        self.actual_label = actual_label

    def get_accuracy(self) -> float:
        return accuracy_score(y_true=self.actual_label, y_pred=self.predicted_label)
