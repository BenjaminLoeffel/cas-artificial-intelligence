import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
from torchvision.utils import save_image
import matplotlib.pyplot as plt
if __name__ == "__main__":
    cuda = torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    print(device)

    training_data = datasets.FashionMNIST(root="data", train=True, download=True, transform=ToTensor())

    test_data = datasets.FashionMNIST(root="data", train=False, download=True, transform=ToTensor())


    class Model(nn.Module):
        def __init__(self, n_features: int):
            super().__init__()
            self.n_features = n_features

            self.encoder = nn.Sequential(nn.Linear(n_features, 128),
                                         nn.ReLU(),
                                         nn.Linear(128, 64),
                                         nn.ReLU(),
                                         nn.Linear(64, 32),
                                         nn.ReLU(),
                                         nn.Linear(32, 16),
                                         nn.ReLU(),
                                         nn.Linear(16, 2), )

            self.decoder = nn.Sequential(nn.Linear(2, 16),
                                         nn.ReLU(),
                                         nn.Linear(16, 32),
                                         nn.ReLU(),
                                         nn.Linear(32, 64),
                                         nn.ReLU(),
                                         nn.Linear(64, 128),
                                         nn.ReLU(),
                                         nn.Linear(128, self.n_features),
                                         nn.Sigmoid())

        def forward(self, x):
            x = self.encoder(x)
            x = self.decoder(x)
            return x


    model = Model(n_features=28 * 28).to(device)
    print(model)


    def train(dataloader, model, loss_fn, optimizer):
        size = len(dataloader.dataset)

        model.train()

        for batch, (X, y) in enumerate(dataloader):
            X = X.reshape(-1, 28 * 28)
            X = X.to(device)
            pred = model(X)
            loss = loss_fn(pred, X)

            # Backpropagation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch % 100 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss:>7f} [{current:>5d}/{size:>5d}]")


    def test(dataloader, model, loss_fn):
        num_batches = len(dataloader)
        model.eval()
        test_loss = 0

        with torch.no_grad():
            for X, y in dataloader:
                X = X.reshape(-1, 28 * 28)
                X = X.to(device)

                pred = model(X)
                test_loss += loss_fn(pred, X).item()

            test_loss /= num_batches
            print(f"Test Error: \n Avg loss: {test_loss:>8f} \n")


    def test_with_testimage(model, test_image, epoch: int, filename: str):
        model.eval()
        with torch.no_grad():
            test_image = test_image.reshape(-1, 28 * 28)
            test_image = test_image.to(device)
            predicted_test_image = model(test_image)
            predicted_test_image = predicted_test_image.reshape(-1, 28, 28)
            save_image(predicted_test_image[0], f"epoch_{epoch}_{filename}.png")


    def save_weights(model: nn.Module, filename: str) -> None:
        torch.save(model.state_dict(), filename)


    loss_fn = nn.MSELoss()

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)  # step, update von params = params - lr * grad

    batch_size = 64

    train_dataloader = DataLoader(training_data, batch_size=batch_size)
    test_dataloader = DataLoader(test_data, batch_size=1, shuffle=True)

    test_image = test_data.data[0, :, :].float()
    save_image(test_image, "testimage.png")

    noise = torch.randn(test_image.shape) * 0.1
    noisy_test_image = torch.add(test_image, noise)
    save_image(noisy_test_image, "noisy_testimage.png")

    for epoch in range(1):
        print(f"Epoch {epoch}\n-------------------------------")
        train(dataloader=train_dataloader, model=model, loss_fn=loss_fn, optimizer=optimizer)
        # test(dataloader=test_dataloader, model=model, loss_fn=loss_fn)
        # test_with_testimage(model=model, test_image=test_image, epoch=epoch, filename="no_noise")
        # test_with_testimage(model=model, test_image=noisy_test_image, epoch=epoch, filename="noise")
        save_weights(model, filename=f"model_epoch_{epoch}.pth")

    a = test_dataloader.dataset.data.reshape(-1, 28* 28)
    a.shape
    a=a.float()
    a=a.to(device)
    encoder_output = model.encoder(a)
    values = encoder_output.cpu().detach().numpy()
    plt.scatter(values[:,0],values[:,1])
    plt.show()