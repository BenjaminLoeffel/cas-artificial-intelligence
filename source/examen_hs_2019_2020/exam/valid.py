class NeuralNetwork(nn.Module):
    def __init__(self, input_size: int):
        super().__init__()
        self.input_size = input_size

        self.linear_relu_stack = nn.Sequential(  # Layer 2..n, einfache Netzwerk übereinander (Sequential)
            nn.Flatten(),
            nn.Linear(self.input_size, 128),  # Input 28x28, Output = 128
            nn.ReLU(),
            nn.Linear(128, 10),  # Input 128, Output = 10 / Predictions
            nn.Softmax())

    def forward(self, x):
        x = self.linear_relu_stack(x)
        return x
