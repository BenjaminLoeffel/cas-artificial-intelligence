class NeuralNetwork(nn.Module):  # 1. Klasse erstellen von nn.Module
    def __init__(self, input_size: int):  # 2. Konstruktor
        super().__init__()  # Super Konstruktor aufrufen
        self.input_size = input_size  # Input 28x28, Output = 512

        self.linear_relu_stack = nn.Sequential(  # Einfache Netzwerk übereinander (Sequential)
            nn.Flatten(),
            nn.Linear(128, self.input_size),
            nn.ReLU(),
            nn.Linear(128, 10),  # Prediction für 10 Kategorien
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.linear_relu_stack(x)
        return x
