import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor

if __name__ == "__main__":
    cuda = torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    print(device)

    training_data = datasets.FashionMNIST(root="data", train=True, download=True, transform=ToTensor())

    test_data = datasets.FashionMNIST(root="data", train=False, download=True, transform=ToTensor())


    class NeuralNetwork(nn.Module):
        def __init__(self, input_size: int):
            super().__init__()
            self.input_size = input_size

            self.linear_relu_stack = nn.Sequential(  # Layer 2..n, einfache Netzwerk übereinander (Sequential)
                nn.Flatten(),
                nn.Linear(self.input_size, 128),  # Input 28x28, Output = 128
                nn.ReLU(),
                nn.Linear(128, 10),  # Input 128, Output = 10 / Predictions
                nn.Softmax())

        def forward(self, x):
            x = self.linear_relu_stack(x)
            return x


    model = NeuralNetwork(input_size=28 * 28).to(device)  # Angabe wo das ausgeführt werden soll
    print(model)


    def train(dataloader, model, loss_fn, optimizer):
        size = len(dataloader.dataset)

        model.train()

        for batch, (X, y) in enumerate(dataloader):
            X, y = X.to(device), y.to(device)
            pred = model(X)
            loss = loss_fn(pred, y)

            # Backpropagation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch % 100 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss:>7f} [{current:>5d}/{size:>5d}]")


    def test(dataloader, model, loss_fn):
        size = len(dataloader.dataset)
        num_batches = len(dataloader)
        model.eval()
        test_loss, correct = 0, 0

        with torch.no_grad():
            for X, y in dataloader:
                X, y = X.to(device), y.to(device)

                pred = model(X)
                test_loss += loss_fn(pred, y).item()
                correct += (pred.argmax(1) == y).type(torch.float).sum().item()

            test_loss /= num_batches
            correct /= size
            print(f"Test Error: \n Accuracy: {(100 * correct):>0.3f}%, Avg loss: {test_loss:>8f} \n")


    loss_fn = nn.CrossEntropyLoss()

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)  # step, update von params = params - lr * grad

    batch_size = 64
    train_dataloader = DataLoader(training_data, batch_size=batch_size)
    test_dataloader = DataLoader(test_data, batch_size=1, shuffle=True)

    for epoch in range(10):
        print(f"Epoch {epoch}\n-------------------------------")
        train(dataloader=train_dataloader, model=model, loss_fn=loss_fn, optimizer=optimizer)
        test(dataloader=test_dataloader, model=model, loss_fn=loss_fn)
