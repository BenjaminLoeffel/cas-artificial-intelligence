import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchvision import datasets, transforms

#
# Setup device (cuda / cpu)
#
cuda = torch.cuda.is_available()
device = torch.device("cuda" if cuda else "cpu")
print("Device:")
print(device)

# convert data to torch.FloatTensor
transform = transforms.ToTensor()  # ToTensor() erstellt proprietäres Format für PyTorch

train_data = datasets.FashionMNIST(
    root="data",
    train=True,
    download=True,
    transform=transform)

test_data = datasets.FashionMNIST(
    root="data",
    train=False,
    download=True,
    transform=transform)

print("#")
print("# Train Data")
print("#")
print(train_data)

print("\n#")
print("# Train Data Classes")
print("#")
print(train_data.classes)
print()

print("\n#")
print("# Train Data ClassToIndex")
print("#")
print(train_data.class_to_idx)

batch_size = 20
train_dataloader = DataLoader(train_data, batch_size=batch_size)
test_dataloader = DataLoader(test_data, batch_size=batch_size, shuffle=True)

for X, y in train_dataloader:
    print("Shape of X [N, C, H, W]: ", X.shape)
    print("Shape of y: ", y.shape, y.dtype)
    break

# specify the image classes
classes = datasets.FashionMNIST.classes
print(classes)

# obtain one batch of training images
dataiter = iter(train_dataloader)
images, labels = dataiter.next()
images = images.numpy()  # convert images to numpy for display

# plot the images in the batch, along with the corresponding labels
fig = plt.figure(figsize=(25, 4))

# display 20 images
for idx in np.arange(20):
    ax = fig.add_subplot(2, 20 // 2, idx + 1, xticks=[], yticks=[])
    img = images[idx][0]
    plt.imshow(img, cmap='gray')
    ax.set_title(classes[labels[idx]])


# define the NN architecture
class ConvAutoencoder(nn.Module):
    def __init__(self):
        super(ConvAutoencoder, self).__init__()

        #
        # encoder layers
        #
        self.conv1 = nn.Conv2d(1, 16, 3, padding=1)  # conv layer (depth from 1 --> 16), 3x3 kernels
        self.conv2 = nn.Conv2d(16, 4, 3, padding=1)  # conv layer (depth from 16 --> 4), 3x3 kernels
        self.pool = nn.MaxPool2d(2, 2)  # pooling layer to reduce x-y dims by two;
        # kernel and stride of 2

        #
        # decoder layers
        #
        self.t_conv1 = nn.ConvTranspose2d(4, 16, 2, stride=2)  # a kernel of 2 and a stride of 2
        self.t_conv2 = nn.ConvTranspose2d(16, 1, 2, stride=2)  # will increase the spatial dims by 2

    def forward(self, x):
        #
        # encode
        #
        x = F.relu(self.conv1(x))  # add hidden layers with relu activation function
        x = self.pool(x)  # and maxpooling after
        x = F.relu(self.conv2(x))  # add second hidden layer
        x = self.pool(x)  # compressed representation
        #
        # decode
        #
        x = F.relu(self.t_conv1(x))  # transpose conv layers, with relu activation function
        x = torch.sigmoid(self.t_conv2(x))  # output layer (with sigmoid for scaling from 0 to 1)

        return x


# initialize the CNN
model = ConvAutoencoder().to(device)
print(model)

# Backward
# - Definition Backward Loss Funktion
# - Build partial derivates of loss function and grads
loss_fn = nn.BCELoss()

# Step
# - Optimizer aktualisiert die Gewichte
# - Der optimizer macht den Update step intelligenter
# - Update: params = params - learing_rate * grad
# - Besser ist Adam optimizer
#   optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)
optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)

# number of epochs to train the model
epochs = 3

for epoch in range(epochs):
    train_loss = 0.0  # monitor training loss

    for data in train_dataloader:  # train the model

        # get images
        images, _ = data  # no need to flatten images
        # _ stands in for labels (y), here
        images = images.to(device)

        # forward pass
        outputs = model(images)  # compute predicted outputs by passing inputs to the model
        loss = loss_fn(outputs, images)  # calculate the loss

        # backward pass
        optimizer.zero_grad()  # clear the gradients of all optimized variables
        loss.backward()  # compute gradient of the loss with respect to model parameters
        optimizer.step()  # perform a single optimization step (parameter update)

        # update running training loss
        train_loss += loss.item() * images.size(0)

    # print avg training statistics
    train_loss = train_loss / len(train_dataloader)
    print('Epoch: {} \tTraining Loss: {:.6f}'.format(epoch, train_loss))

print("Done!")

# obtain one batch of test images
dataiter = iter(test_dataloader)
images, labels = dataiter.next()
images = images.to(device)

# get sample outputs
output = model(images)
# prep images for display
images = images.cpu().numpy()

# output is resized into a batch of iages
output = output.view(batch_size, 1, 28, 28)
# use detach when it's an output that requires_grad
output = output.detach().cpu().numpy()


# # plot the first ten input images and then reconstructed images
# fig, axes = plt.subplots(nrows=2, ncols=10, sharex=True, sharey=True, figsize=(24,4))

# # input images on top row, reconstructions on bottom
# for images, row in zip([images, output], axes):
#     for img, ax in zip(images, row):
#         ax.imshow(np.squeeze(img))
#         ax.get_xaxis().set_visible(False)
#         ax.get_yaxis().set_visible(False)

# plot the first ten input images and then reconstructed images
def plot_images(images, title: str, n=20, nrows=2, ncols=10, sharex=True, sharey=True, figsize=(24, 4)):
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, sharex=sharex, sharey=sharey, figsize=figsize)
    for idx in np.arange(n):
        ax = fig.add_subplot(nrows, ncols, idx + 1, xticks=[], yticks=[])
        img = images[idx][0]
        plt.imshow(img, cmap='gray')
        ax.set_title(classes[labels[idx]])
    fig.savefig(f"{title}.png")


plot_images(images, title="original", n=20, nrows=2, ncols=10, sharex=True, sharey=True, figsize=(24, 4))
plot_images(output, title="reconstructed", n=20, nrows=2, ncols=10, sharex=True, sharey=True, figsize=(24, 4))
