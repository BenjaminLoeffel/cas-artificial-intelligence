import re
import subprocess


class GpuStatus:
    _temperature: float
    _used_memory: float

    def __init__(self, temperature: float, used_memory: float):
        self._temperature = temperature
        self._used_memory = used_memory

    @property
    def temperature(self) -> float:
        return self._temperature

    @property
    def used_memory(self) -> float:
        return self._used_memory


class GpuMonitor:

    def __init__(self):
        pass

    def get_status(self) -> GpuStatus:
        output = subprocess.check_output("nvidia-smi -q -d temperature,memory").decode("utf8")
        lines = output.split("\r\n")
        used_memory = 0
        memory_found = False
        temperature = 0
        temperature_found = False
        for line in lines:
            if "Used" in line and not memory_found:
                parts = line.split(":")
                used_memory = float(re.findall(r'\d+', parts[1])[0])
                memory_found = True
            if "GPU Current Temp" in line and not temperature_found:
                parts = line.split(":")
                temperature = float(re.findall(r'\d+', parts[1])[0])
                break
        return GpuStatus(temperature=temperature, used_memory=used_memory)
