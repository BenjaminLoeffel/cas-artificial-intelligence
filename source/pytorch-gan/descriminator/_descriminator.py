import torch.nn as nn


class Discriminator(nn.Module):
    _nc: int
    _ndf: int

    def __init__(self, nc: int, ndf: int):
        super().__init__()

        self._nc = nc
        self._ndf = ndf

        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(self._nc, self._ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, self._ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(self._ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(self._ndf * 2, self._ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(self._ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(self._ndf * 4, self._ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(self._ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(self._ndf * 8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.main(input)
