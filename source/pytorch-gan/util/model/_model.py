import datetime
import logging
from pathlib import Path

import torch
from torch.nn import Module


class ModelRepository:
    _model_path: Path

    def __init__(self, model_path: Path):
        self._model_path = model_path
        self._model_path.mkdir(parents=True, exist_ok=True)

    def save_model_with_name_and_timestamp(self, model: Module, name: str):
        filepath = self._model_path.joinpath(f"{name}_{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}_state_dict")
        filepath.parent.mkdir(parents=True, exist_ok=True)
        torch.save(model.state_dict(), filepath)
        logging.info(f"successfully saved model to {filepath}")
