import logging
from pathlib import Path

import numpy as np
from PIL import Image

_logger = logging.getLogger(__file__)


class ImageExporter:
    _image_path: Path

    def __init__(self, image_path: Path):
        self._image_path = image_path

    def export(self, image: np.ndarray, name: str):
        self._image_path.mkdir(parents=True, exist_ok=True)
        image_file = self._image_path.joinpath(f"{name}.png")
        pil_image = Image.fromarray(np.transpose(np.uint8(image * 255)))
        pil_image = pil_image.rotate(-90)
        with open(image_file, "wb") as f:
            pil_image.save(f)
        _logger.info(f"exported image to {image_file}")
