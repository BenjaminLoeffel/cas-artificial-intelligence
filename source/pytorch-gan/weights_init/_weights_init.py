from torch import nn


def weights_init(model: nn.Module):
    """
    From the DCGAN paper, the authors specify that all model weights shall
    be randomly initialized from a Normal distribution with mean=0,
    stdev=0.2. The ``weights_init`` function takes an initialized model as
    input and reinitializes all convolutional, convolutional-transpose, and
    batch normalization layers to meet this criteria. This function is
    applied to the models immediately after initialization.

    # custom weights initialization called on netG and netD
    :param m:
    :return:
    """
    classname = model.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(model.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(model.weight.data, 1.0, 0.02)
        nn.init.constant_(model.bias.data, 0)
