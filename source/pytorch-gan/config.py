import logging
import random
from pathlib import Path

import numpy as np
import torch

_logger = logging.getLogger(__file__)


class Config:
    _tensorboard_path: Path
    _models_path: Path
    _image_folder_path: Path
    _generate_images: Path
    _image_size: int
    _nc: int
    _ngf: int
    _nz: int
    _ndf: int
    _beta1: float
    _learning_rate: float

    def __init__(self, tensorboard_path: Path, models_path: Path, image_folder_path: Path, generate_images: Path,
                 image_size: int, nz: int,
                 nc: int, ngf: int, ndf: int, beta1: float, learning_rate: float):
        self._tensorboard_path = tensorboard_path
        self._models_path = models_path
        self._image_folder_path = image_folder_path
        self._generate_images = generate_images
        self._image_size = image_size
        self._nz = nz
        self._nc = nc
        self._ngf = ngf
        self._ndf = ndf
        self._beta1 = beta1
        self._learning_rate = learning_rate

    @property
    def tensorboard_path(self) -> Path:
        return self._tensorboard_path

    @property
    def models_path(self) -> Path:
        return self._models_path

    @property
    def image_folder_path(self) -> Path:
        return self._image_folder_path

    @property
    def generate_images(self) -> Path:
        return self._generate_images

    @property
    def image_size(self) -> int:
        return self._image_size

    @property
    def nc(self) -> int:
        return self._nc

    @property
    def ngf(self) -> int:
        return self._ngf

    @property
    def nz(self) -> int:
        return self._nz

    @property
    def ndf(self) -> int:
        return self._ndf

    @property
    def beta1(self) -> float:
        return self._beta1

    @property
    def learning_rate(self) -> float:
        return self._learning_rate


def get_config():
    seed = 0
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    _logger.info(f"seeded all random stuff with {seed}")

    return Config(tensorboard_path=Path.cwd().joinpath("tensorboard_logs"),
                  models_path=Path.cwd().joinpath("saved_models"),
                  image_folder_path=Path.cwd().joinpath("celeba"),
                  generate_images=Path.cwd().joinpath("generated_images"),
                  image_size=64,
                  nz=100,
                  nc=3,
                  ngf=64,
                  ndf=64,
                  beta1=0.5,
                  learning_rate=0.0002)


current_config = get_config()
