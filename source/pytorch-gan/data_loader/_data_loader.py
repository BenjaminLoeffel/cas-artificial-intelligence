from torch.utils.data import DataLoader, Dataset


class DataLoaderRepository:
    _dataset: Dataset
    _batch_size: int
    _shuffle: bool
    _n_workers: int

    def __init__(self, dataset: Dataset, batch_size: int, shuffle: bool = True, n_workers: int = 4):
        self._dataset = dataset
        self._batch_size = batch_size
        self._shuffle = shuffle
        self._n_workers = n_workers

    def get_dataloader(self) -> DataLoader:
        return DataLoader(dataset=self._dataset,
                          batch_size=self._batch_size,
                          shuffle=self._shuffle,
                          num_workers=self._n_workers)
