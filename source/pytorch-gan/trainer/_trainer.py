import logging

import torch
from torch import device
from torch import nn
from torch.optim import Optimizer
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from descriminator import Discriminator
from generator import Generator
from generator import ImageGenerator
from gpu_monitor import GpuMonitor
from noise_generator import NoiseGenerator
from util.image import ImageExporter
from util.model import ModelRepository

_logger = logging.getLogger(__file__)


class Trainer:
    _device: device
    _loss_function: nn.BCELoss
    _epochs: int
    _dataloader: DataLoader

    _generator: Generator
    _generator_optimizer: Optimizer

    _descriminator: Discriminator
    _descriminator_optimizer: Optimizer

    _real_label: float = 1.0
    _fake_label: float = 0.0

    _fixed_noise: torch.Tensor
    _noise_generator: NoiseGenerator
    _nz: int

    _summary_writer: SummaryWriter
    _gpu_monitor: GpuMonitor

    _model_repository: ModelRepository
    _image_exporter: ImageExporter

    def __init__(self, device: device, loss_function: nn.BCELoss, epochs: int, dataloader: DataLoader,
                 generator: Generator, generator_optimizer: Optimizer, descriminator: Discriminator,
                 descriminator_optimizer: Optimizer, nz: int, fixed_noise: torch.Tensor,
                 noise_generator: NoiseGenerator, summary_writer: SummaryWriter, gpu_monitor: GpuMonitor,
                 model_repository: ModelRepository, image_exporter: ImageExporter):
        self._device = device
        self._loss_function = loss_function
        self._dataloader = dataloader
        self._epochs = epochs

        self._generator = generator
        self._generator_optimizer = generator_optimizer

        self._descriminator = descriminator
        self._descriminator_optimizer = descriminator_optimizer

        self._nz = nz

        self._fixed_noise = fixed_noise
        self._noise_generator = noise_generator

        self._summary_writer = summary_writer
        self._gpu_monitor = gpu_monitor
        self._model_repository = model_repository
        self._image_exporter = image_exporter

    def train(self):
        n_batches = len(self._dataloader)
        step: int = 0
        for index_epoch in range(self._epochs):
            for index_batch, data in enumerate(self._dataloader, 0):
                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                self._descriminator.zero_grad()
                # Format batch
                real_cpu = data[0].to(self._device)
                b_size = real_cpu.size(0)
                label = torch.full((b_size,), self._real_label, device=self._device)
                # Forward pass real batch through D
                output = self._descriminator(real_cpu).view(-1)
                # Calculate loss on all-real batch
                errD_real = self._loss_function(output, label)
                # Calculate gradients for D in backward pass
                errD_real.backward()
                D_x = output.mean().item()

                ## Train with all-fake batch
                # Generate batch of latent vectors
                noise = torch.randn(b_size, self._nz, 1, 1, device=self._device)
                # Generate fake image batch with G
                fake = self._generator(noise)
                label.fill_(self._fake_label)
                # Classify all fake batch with D
                output = self._descriminator(fake.detach()).view(-1)
                # Calculate D's loss on the all-fake batch
                errD_fake = self._loss_function(output, label)
                # Calculate the gradients for this batch
                errD_fake.backward()
                D_G_z1 = output.mean().item()
                # Add the gradients from the all-real and all-fake batches
                errD = errD_real + errD_fake
                # Update D
                self._descriminator_optimizer.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self._generator.zero_grad()
                label.fill_(self._real_label)  # fake labels are real for generator cost
                # Since we just updated D, perform another forward pass of all-fake batch through D

                output = self._descriminator(fake).view(-1)
                # Calculate G's loss based on this output
                errG = self._loss_function(output, label)
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self._generator_optimizer.step()

                discriminator_loss = errD.item()
                generator_loss = errG.item()

                gpu_status = self._gpu_monitor.get_status()

                _logger.info(
                    f"[{index_epoch}/{self._epochs}][{index_batch}/{n_batches}]\tLoss_D: {discriminator_loss:.4f}\tLoss_G: {generator_loss:.4f}\tD(x): {D_x:.4f}\tD(G(z)): {D_G_z1:.4f} / {D_G_z2:.4f}")
                _logger.info(
                    f"gpu temperature: {gpu_status.temperature} °C, gpu memory used: {gpu_status.used_memory} Mb")

                # Save Losses for plotting later
                self._summary_writer.add_scalar("generator/loss", generator_loss, step)
                self._summary_writer.add_scalar("descriminator/loss", discriminator_loss, step)

                self._summary_writer.add_scalar("gpu/temperature", gpu_status.temperature, step)
                self._summary_writer.add_scalar("gpu/used_memory", gpu_status.used_memory, step)

                # Check how the generator is doing by saving G's output on fixed_noise
                if (step % 500 == 0):
                    image_generator = ImageGenerator(input=self._fixed_noise,
                                                     generator=self._generator)
                    generated_image = image_generator.generate_image()
                    self._image_exporter.export(image=generated_image, name=f"fixed_noise_step_{step}")

                    noise = self._noise_generator.get_noise()
                    image_generator = ImageGenerator(input=noise,
                                                     generator=self._generator)
                    generated_image = image_generator.generate_image()
                    self._image_exporter.export(image=generated_image, name=f"random_noise_step_{step}")

                step += 1

            image_generator = ImageGenerator(input=self._fixed_noise,
                                             generator=self._generator)
            generated_image = image_generator.generate_image()
            self._image_exporter.export(image=generated_image, name=f"fixed_noise_step_{step}_epoch_{index_epoch}")
            noise = self._noise_generator.get_noise()

            image_generator = ImageGenerator(input=noise,
                                             generator=self._generator)
            generated_image = image_generator.generate_image()
            self._image_exporter.export(image=generated_image, name=f"random_noise_step_{step}_epoch_{index_epoch}")

            self._model_repository.save_model_with_name_and_timestamp(model=self._generator,
                                                                      name=f"generator_epoch_{index_epoch}")
            self._model_repository.save_model_with_name_and_timestamp(model=self._descriminator,
                                                                      name=f"descriminator_epoch_{index_epoch}")
            _logger.info(f"finished epoch{index_epoch}/{self._epochs}")
