import torch
from torch import device


class NoiseGenerator:
    _device: device
    _width: int
    _height: int

    def __init__(self, device: device, width: int, height: int):
        self._device = device
        self._width = width
        self._height = height

    def get_noise(self) -> torch.Tensor:
        return torch.randn(self._height, self._width, 1, 1, device=self._device)
