from typing import Callable

import torchvision.transforms as transforms


class TransformRepository:
    _image_size: int

    def __init__(self, image_size: int):
        self._image_size = image_size

    def get_transform(self) -> Callable:
        transform = transforms.Compose([
            transforms.Resize(self._image_size),
            transforms.CenterCrop(self._image_size),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
        return transform
