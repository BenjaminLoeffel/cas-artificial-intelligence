import numpy as np
import torch
import torch.nn as nn
from torchvision.utils import make_grid


class Generator(nn.Module):
    _nc: int
    _nz: int
    _ngf: int

    def __init__(self, nc: int, nz: int, ngf: int):
        """
        :param nc: Number of channels in the training images. For color images this is 3
        :param nz: Size of z latent vector (i.e. size of generator input)
        :param ngf: Size of feature maps in generator
        """
        super().__init__()
        self._nc = nc
        self._nz = nz
        self._ngf = ngf

        self.main = nn.Sequential(
            # input is Z, going into a convolution
            nn.ConvTranspose2d(nz, ngf * 8, 4, 1, 0, bias=False),
            nn.BatchNorm2d(ngf * 8),
            nn.ReLU(True),
            # state size. (ngf*8) x 4 x 4
            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),
            # state size. (ngf*4) x 8 x 8
            nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),
            # state size. (ngf*2) x 16 x 16
            nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),
            # state size. (ngf) x 32 x 32
            nn.ConvTranspose2d(ngf, nc, 4, 2, 1, bias=False),
            nn.Tanh()
            # state size. (nc) x 64 x 64
        )

    def forward(self, input):
        return self.main(input)


class ImageGenerator:
    _input: torch.Tensor
    _generator: Generator

    def __init__(self, input: torch.Tensor, generator: Generator):
        self._input = input
        self._generator = generator

    def generate_image(self) -> np.ndarray:
        with torch.no_grad():
            fake = self._generator(self._input).detach().cpu()
        return make_grid(fake, padding=2, normalize=True).numpy()
