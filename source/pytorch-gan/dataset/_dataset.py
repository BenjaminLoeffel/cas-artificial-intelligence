from pathlib import Path
from typing import Optional, Callable

from torchvision.datasets import ImageFolder


class DatasetRepository:
    _image_folder_path: Path
    _transform: Optional[Callable]

    def __init__(self, image_folder_path: Path, transform: Optional[Callable]):
        self._image_folder_path = image_folder_path
        self._transform = transform

    def get_dataset(self) -> ImageFolder:
        return ImageFolder(root=str(self._image_folder_path),
                           transform=self._transform)
