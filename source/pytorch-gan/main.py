import datetime
import logging

import torch.utils.data
from torch import nn
from torch.optim import Adam

from config import get_config
from data_loader import DataLoaderRepository
from dataset import DatasetRepository
from descriminator import Discriminator
from generator import Generator
from gpu_monitor import GpuMonitor
from logging_util.builder import LoggingBuilder
from noise_generator import NoiseGenerator
from summary_writer import SummaryWriterRepository
from trainer import Trainer
from transform import TransformRepository
from util.image import ImageExporter
from util.model import ModelRepository
from weights_init import weights_init

_logger = logging.getLogger()
builder = LoggingBuilder(_logger)
builder.add_default_loggers(__file__)

if __name__ == "__main__":
    try:
        _logger.info("main started")

        current_config = get_config()

        # device and datatype
        _logger.info(f"cuda available: {torch.cuda.is_available()}")
        cuda = torch.cuda.is_available()
        device = torch.device("cuda" if cuda else "cpu")

        experiment_name = f"experiment_{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}"

        # get transformation
        transform_repository = TransformRepository(image_size=current_config.image_size)
        transform = transform_repository.get_transform()

        # get dataset
        dataset_respository = DatasetRepository(image_folder_path=current_config.image_folder_path,
                                                transform=transform)
        dataset = dataset_respository.get_dataset()

        # get dataloader
        dataloader_repository = DataLoaderRepository(dataset=dataset,
                                                     batch_size=128,
                                                     shuffle=True,
                                                     n_workers=4)
        data_loader = dataloader_repository.get_dataloader()

        # get generator
        generator = Generator(nc=current_config.nc,
                              nz=current_config.nz,
                              ngf=current_config.ngf)
        generator.to(device)
        generator.apply(weights_init)
        generator_optimizer = Adam(generator.parameters(), lr=current_config.learning_rate,
                                   betas=(current_config.beta1, 0.999))

        # get descriminator
        descriminator = Discriminator(nc=current_config.nc,
                                      ndf=current_config.ndf)
        descriminator.to(device)
        descriminator.apply(weights_init)
        descrimimator_optimizer = Adam(descriminator.parameters(), lr=current_config.learning_rate,
                                       betas=(current_config.beta1, 0.999))

        # get noise generator
        noise_generator = NoiseGenerator(device=device,
                                         width=current_config.nz,
                                         height=current_config.image_size)
        fixed_noise = noise_generator.get_noise()

        # get summary writer for tensorboard
        summary_writer_repository = SummaryWriterRepository(
            log_path=current_config.tensorboard_path.joinpath(experiment_name))
        summary_writer = summary_writer_repository.get_writer()

        # get gpu_monitor
        gpu_monitor = GpuMonitor()

        # get model repository
        model_repository = ModelRepository(model_path=current_config.models_path.joinpath(experiment_name))

        # get image exporter
        image_exporter = ImageExporter(image_path=current_config.generate_images.joinpath(experiment_name))

        # get loss function
        loss_function = nn.BCELoss()

        # get trainer
        trainer = Trainer(device=device,
                          loss_function=loss_function,
                          epochs=50,
                          dataloader=data_loader,
                          generator=generator,
                          generator_optimizer=generator_optimizer,
                          descriminator=descriminator,
                          descriminator_optimizer=descrimimator_optimizer,
                          nz=current_config.nz,
                          fixed_noise=fixed_noise,
                          noise_generator=noise_generator,
                          summary_writer=summary_writer,
                          gpu_monitor=gpu_monitor,
                          model_repository=model_repository,
                          image_exporter=image_exporter)

        # train
        trainer.train()

    except Exception as e:
        _logger.exception(f"exception occurred {e}", exc_info=True)

    finally:
        _logger.info("main finished")
