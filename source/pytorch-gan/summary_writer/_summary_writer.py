from pathlib import Path

from torch.utils.tensorboard import SummaryWriter


class SummaryWriterRepository:
    _log_path: Path

    def __init__(self, log_path: Path):
        self._log_path = log_path

    def get_writer(self) -> SummaryWriter:
        return SummaryWriter(log_dir=str(self._log_path))
