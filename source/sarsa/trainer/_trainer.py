from gym.wrappers.order_enforcing import OrderEnforcing
from tensorflow.python.ops.summary_ops_v2 import SummaryWriter


class SarsaTrainer:
    env:OrderEnforcing

    def __init__(self,env:OrderEnforcing):
        self.env=env



    def train(self,n_epsiodes:int,train_summary_writer: SummaryWriter):
        for i_episode in range(n_epsiodes):
            score = 0  # initialize score
            state = self.env.reset()  # start episode

            # eps = 1.0 / i_episode
            eps = max(eps * eps_decay, eps_min)
            # print(eps)                                # set value of epsilon
            action = epsilon_greedy(Q, state, nA, eps)  # epsilon-greedy action selection

            while True: