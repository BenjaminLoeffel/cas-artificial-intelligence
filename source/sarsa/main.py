import datetime
import logging
import random
import sys
from collections import defaultdict, deque

import gym
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.python.ops.summary_ops_v2 import SummaryWriter

from config import get_config
from logging_util.builder import LoggingBuilder
from plot_utils import plot_values


def update_Q_sarsa(alpha, gamma, Q, state, action, reward, next_state=None, next_action=None):
    """Returns updated Q-value for the most recent experience."""
    current = Q[state][action]  # estimate in Q-table (for current state, action pair)
    # get value of state, action pair at next time step
    Qsa_next = Q[next_state][next_action] if next_state is not None else 0
    target = reward + (gamma * Qsa_next)  # construct TD target
    new_value = current + (alpha * (target - current))  # get updated value
    return new_value


def epsilon_greedy(Q, state, nA, eps):
    """Selects epsilon-greedy action for supplied state.

    Params
    ======
        Q (dictionary): action-value function
        state (int): current state
        nA (int): number actions in the environment
        eps (float): epsilon
    """
    if random.random() > eps:  # select greedy action with probability epsilon
        return np.argmax(Q[state])
    else:  # otherwise, select an action randomly
        return random.choice(np.arange(env.action_space.n))


def sarsa(env, num_episodes: int, alpha, train_summary_writer: SummaryWriter, gamma=1.0, plot_every=100):
    nA = env.action_space.n  # number of actions
    Q = defaultdict(lambda: np.zeros(nA))  # initialize empty dictionary of arrays

    eps_decay = .99999
    eps = 1.
    eps_min = .05

    # monitor performance
    tmp_scores = deque(maxlen=plot_every)  # deque for keeping track of scores

    for i_episode in range(1, num_episodes + 1):
        # monitor progress
        if i_episode % 100 == 0:
            print("\rEpisode {}/{}".format(i_episode, num_episodes), end="")
            sys.stdout.flush()
        score = 0  # initialize score
        state = env.reset()  # start episode

        # eps = 1.0 / i_episode
        eps = max(eps * eps_decay, eps_min)
        # print(eps)                                # set value of epsilon
        action = epsilon_greedy(Q, state, nA, eps)  # epsilon-greedy action selection

        while True:
            next_state, reward, done, info = env.step(action)  # take action A, observe R, S"
            score += reward  # add reward to agent"s score
            if not done:
                next_action = epsilon_greedy(Q, next_state, nA, eps)  # epsilon-greedy action
                Q[state][action] = update_Q_sarsa(alpha, gamma, Q, state, action, reward, next_state, next_action)

                state = next_state  # S <- S"
                action = next_action  # A <- A"
            if done:
                Q[state][action] = update_Q_sarsa(alpha, gamma, Q, state, action, reward)
                tmp_scores.append(score)  # append score
                break

        if (i_episode % plot_every == 0):
            avg_score = np.mean(tmp_scores)
            with train_summary_writer.as_default():
                tf.summary.scalar("train/avg_score", avg_score, step=i_episode)
    return Q


_logger = logging.getLogger()
builder = LoggingBuilder(_logger)
builder.add_default_loggers(__file__)

if __name__ == "__main__":
    try:
        _logger.info("main started")

        current_config = get_config()

        env = gym.make("CliffWalking-v0")
        # env = gym.make("FrozenLake-v0", is_slippery=False)

        print(env.action_space)
        print(env.observation_space)

        experiment = f"experiment_{datetime.datetime.now().strftime('%Y%m%d-%H%M%S')}"

        tensorboard_log_path = current_config.tensorboard_path.joinpath(experiment)
        train_summary_writer = tf.summary.create_file_writer(str(tensorboard_log_path))

        # define the optimal state-value function
        V_opt = np.zeros((4, 12))
        V_opt[0][0:13] = -np.arange(3, 15)[::-1]
        V_opt[1][0:13] = -np.arange(3, 15)[::-1] + 1
        V_opt[2][0:13] = -np.arange(3, 15)[::-1] + 2
        V_opt[3][0] = -13

        # plot_values(V_opt)

        # obtain the estimated optimal policy and corresponding action-value function
        Q_sarsa = sarsa(env=env,
                        num_episodes=50_000,
                        alpha=0.01,
                        train_summary_writer=train_summary_writer)

        # print the estimated optimal policy
        # policy_sarsa = np.array([np.argmax(Q_sarsa[key]) if key in Q_sarsa else -1 for key in np.arange(48)]).reshape(4,12)
        policy_sarsa = np.array([np.argmax(Q_sarsa[key]) if key in Q_sarsa else -1 for key in np.arange(16)]).reshape(4,
                                                                                                                      4)
        # check_test.run_check("td_control_check", policy_sarsa)
        print("\nEstimated Optimal Policy (UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3, N/A = -1):")
        print(policy_sarsa)

        # plot the estimated optimal state-value function
        # V_sarsa = ([np.max(Q_sarsa[key]) if key in Q_sarsa else 0 for key in np.arange(48)])
        # plot_values(V_sarsa)

    except Exception as e:
        _logger.exception(f"exception occurred {e}", exc_info=True)

    finally:
        _logger.info("main finished")
