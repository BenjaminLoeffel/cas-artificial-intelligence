import logging

import matplotlib.pyplot as plt
import torch

from dataset import Dataset

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)


def main():
    # try:
    logging.info("main started")

    logging.info(f"cuda available: {torch.cuda.is_available()}")

    dataset = Dataset()

    train_labels, train_features, test_labels, test_features = dataset.get_train_test()

    plt.close("all")

    plt.show()

    # except Exception as e:
    #     logging.exception(f"exception occurred {e}", exc_info=True)
    #
    # finally:
    #     logging.info("main finished")


if __name__ == "__main__":
    main()
