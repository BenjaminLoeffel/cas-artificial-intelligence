import json
from pathlib import Path
from typing import List


class ResultRepository:

    @staticmethod
    def save_test_result(path: Path, result: List[dict], name: str):
        path.mkdir(parents=True, exist_ok=True)
        filepath = path.joinpath(f"{name}.json")
        with open(filepath, "w") as f:
            json.dump(result, f)
