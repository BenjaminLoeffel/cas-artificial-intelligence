# =====================================================================================================================
# Aufgabe S&P 500 Classifier with PyTorch Lightning
# 20.11.2021, Thomas Iten
# =====================================================================================================================
# Special thanks to Thomas Iten, see his github repository at: https://github.com/surfmachine/ai
import datetime
import logging
import os
import os.path
from pathlib import Path
from typing import Optional, Tuple

import bs4 as bs
import numpy as np
import pandas as pd
import pytorch_lightning as pl
import requests
import torch
import yfinance
from torch.utils.data import DataLoader, TensorDataset


class SP500DataModule(pl.LightningDataModule):
    """Data module for the "Standard and Poor 500" company performance data.
    Usage:
    (1) The recommended way to use a DataModule is as follows:
        dm = SP500DataModule()
        model = Model()
        trainer.fit(model, dm)
        trainer.test(datamodule=dm)
    (2) If you need information from the dataset to build your model, then run prepare_data() and setup() manually
        (Lightning ensures the method runs on the correct devices):
        dm = SP500DataModule()
        dm.prepare_data()
        dm.setup(stage="fit")
        model = Model(num_classes=dm.num_classes, width=dm.width, vocab=dm.vocab)
        trainer.fit(model, dm)
        dm.setup(stage="test")
        trainer.test(datamodule=dm)
    Further details see:
    - https://pytorch-lightning.readthedocs.io/en/latest/starter/introduction_guide.html
    - https://pytorch-lightning.readthedocs.io/en/latest/extensions/datamodules.html
    """
    URL: str = "http://en.wikipedia.org/wiki/List_of_S%26P_500_companies"

    _train_val_test_split: Optional[Tuple[int, int, int]]
    _start_time: datetime.datetime
    _stop_time: datetime.datetime
    _data_path: Path
    _file_path: Path
    _force_download: bool

    _spy_binary: bool
    _batch_size: int

    def __init__(self, batch_size: int = 32, train_val_test_split: Optional[Tuple[int, int, int]] = None,
                 spy_binary: bool = True, data_path: Optional[Path] = None, force_download: bool = False):
        super().__init__()

        if train_val_test_split is None:
            train_val_test_split = [75, 15, 10]
        self._train_val_test_split = train_val_test_split

        # download properties
        self._start_time = datetime.datetime(2010, 1, 1)
        self._stop_time = datetime.datetime.now()
        if data_path is None:
            data_path = Path(__file__).parent
        self._data_path = data_path
        self._file_path = self._data_path.joinpath("sp500.csv")
        self._force_download = force_download

        # prepare and transform properties
        self._spy_binary = spy_binary
        self._batch_size = batch_size

        # data
        self._data: Optional[TensorDataset] = None
        self._train_dataset: Optional[TensorDataset] = None
        self._val_dataset: Optional[TensorDataset] = None
        self._test_dataset: Optional[TensorDataset] = None

    @property
    def data(self) -> Optional[TensorDataset]:
        return self._data

    def prepare_data(self):
        """Download and prepare the SP500 data."""
        df = self._download_sp500()
        if self._spy_binary:
            df.SPY = [1 if spy > 0 else 0 for spy in df.SPY]
        self._data = df

    def setup(self, stage: Optional[str] = None):
        """Setup the data according the stage "fit" or "test". If the stage is none, setup the data for all stages."""

        # Split data into test, val and train dataframes
        train_percent, val_percent, test_percent = self._train_val_test_split
        rows = self._data.shape[0]
        test_rows = int(test_percent * rows / 100)
        val_rows = int(val_percent * rows / 100)
        train_rows = rows - test_rows - val_rows

        logging.info("Setup - define data split:")
        logging.info("- Train rows : {0:04d} ( {1}%)".format(train_rows, train_percent))
        logging.info("- Val   rows : {0:04d} ( {1}%)".format(val_rows, val_percent))
        logging.info("- Test  rows : {0:04d} ( {1}%)".format(test_rows, test_percent))
        logging.info("- Total rows : {0:04d} (100%)".format(rows))

        logging.info("Setup - split and transform data for stage: {}".format(stage))
        if stage == "fit" or stage is None:
            train_data = self._data.iloc[:train_rows]
            val_data = self._data.iloc[train_rows:train_rows + val_rows]
            logging.info("- Train shape: {}".format(train_data.shape))
            logging.info("- Val   shape: {}".format(val_data.shape))
            # transform data and assign properties to use in data loaders
            self._train_dataset = self._transform_to_dataset(train_data)
            self._val_dataset = self._transform_to_dataset(val_data)

        if stage == "test" or stage is None:
            test_data = self._data.iloc[train_rows + val_rows:]
            logging.info("- Test  shape: {}".format(test_data.shape))
            # transform data and assign properties to use in data loaders
            self._test_dataset = self._transform_to_dataset(test_data)

        logging.info("- Total shape: {}".format(self._data.shape))

    def train_dataloader(self) -> DataLoader:
        return DataLoader(self._train_dataset, batch_size=self._batch_size)

    def val_dataloader(self) -> DataLoader:
        return DataLoader(self._val_dataset, batch_size=self._batch_size)

    def test_dataloader(self) -> DataLoader:
        return DataLoader(self._test_dataset, batch_size=self._batch_size)

    def _transform_to_dataset(self, df) -> TensorDataset:

        # Split labels and features
        labels = df.SPY.values
        features = df.iloc[:, :-1].values

        # Convert to tensor
        tensor_labels = torch.tensor(labels).unsqueeze(1).float()
        tensor_features = torch.tensor(features).float()

        # Create tensor dataset
        return TensorDataset(tensor_features, tensor_labels)

    def _download_sp500(self) -> pd.DataFrame:
        """Download the SP500 data from the internet, save data to a csv file and return the result.
        Notes:
        - All further calls will serve the data from the csv file.
        - To trigger a new download from the internet, set the force_download flag to True.
        """

        # Load data from file
        if os.path.isfile(self._file_path) and not self._force_download:
            logging.info(f"Load SP500 from file: {self._file_path}")
            return pd.read_csv(self._file_path, index_col=0, parse_dates=True)

        # Download data
        logging.info(f"Download data from: {SP500DataModule.URL}")
        resp = requests.get(SP500DataModule.URL)
        soup = bs.BeautifulSoup(resp.text, "lxml")
        table = soup.find("table", {"class": "wikitable sortable"})

        tickers = []
        for row in table.findAll("tr")[1:]:
            ticker = row.findAll("td")[0].text
            tickers.append(ticker)

        tickers = [s.replace("\n", "") for s in tickers]
        data = yfinance.download(tickers, start=self._start_time, end=self._stop_time)
        df_data = data["Adj Close"]

        df_spy = yfinance.download("SPY", start=self._start_time, end=self._stop_time)
        df_spy = df_spy.loc[:, ["Adj Close"]]
        df_spy.columns = ["SPY"]

        df = pd.concat([df_data, df_spy], axis=1)

        # Prepare data
        df.dropna(axis=0, how="all", inplace=True)
        logging.info(
            f"Dropping columns due to nans > 50%: {df.loc[:, list((100 * (df.isnull().sum() / len(df.index)) > 50))].columns}")
        df = df.drop(df.loc[:, list((100 * (df.isnull().sum() / len(df.index)) > 50))].columns, 1)
        df = df.ffill().bfill()
        logging.info(f"Any columns still contain nans:  {df.isnull().values.any()}")

        df_returns = pd.DataFrame()
        for name in df.columns:
            df_returns[name] = np.log(df[name]).diff()

        df_returns.dropna(axis=0, how="any", inplace=True)

        # Save data and return result
        logging.info(f"Save data to file: {self._file_path}")
        df_returns.to_csv(self._file_path)

        self._data = df_returns
        return self._data


if __name__ == "__main__":
    dm = SP500DataModule()
    dm.prepare_data()
    logging.info(dm.data.head())
    dm.setup()
