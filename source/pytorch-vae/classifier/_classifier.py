import pytorch_lightning as pl
import torch
import torchmetrics
from torch import Tensor
from torch import nn
from torch.nn import functional as F


class LitBinClassifier(pl.LightningModule):
    lr: float

    def __init__(self):
        super().__init__()
        self.linear_relu_stack = nn.Sequential(nn.Linear(489, 512), nn.ReLU(), nn.Linear(512, 512), nn.ReLU(),
                                               nn.Linear(512, 512), nn.ReLU(), nn.Linear(512, 1))
        self.lr = 0.0001

    def training_step(self, batch: Tensor, batch_idx: int):
        x, y = batch
        # x_augmented, _, _ = self.vae(x)
        # if batch_idx % 5 == 0:
        #     y_hat = self.linear_relu_stack(x)
        # else:
        #     y_hat = self.linear_relu_stack(x_augmented)

        y_hat = self.linear_relu_stack(x)
        loss = F.binary_cross_entropy_with_logits(y_hat, y)

        y_hat = (y_hat > 0.5).int()
        metric = torchmetrics.Accuracy()
        metric.to(self.device)
        acc = metric(y_hat, y.int())
        self.log("train_loss", loss)
        self.log("train_accuracy", acc)
        return loss

    def validation_step(self, batch: Tensor, batch_idx: int):
        x, y = batch
        y_hat = self.linear_relu_stack(x)

        loss = F.binary_cross_entropy_with_logits(y_hat, y)
        y_hat = (y_hat > 0.5).int()
        metric = torchmetrics.Accuracy()
        metric.to(self.device)
        acc = metric(y_hat, y.int())
        self.log("val_loss", loss)
        self.log("val_accuracy", acc)

    def test_step(self, batch: Tensor, batch_idx: int):
        x, y = batch
        y_hat = self.linear_relu_stack(x)

        loss = F.binary_cross_entropy_with_logits(y_hat, y)
        y_hat = (y_hat > 0.5).int()
        metric = torchmetrics.Accuracy()
        metric.to(self.device)
        acc = metric(y_hat, y.int())
        self.log("test_loss", loss)
        self.log("test_accuracy", acc)

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.parameters(), lr=self.lr)
        return optimizer
