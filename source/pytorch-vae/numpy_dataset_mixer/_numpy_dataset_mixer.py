from abc import ABC, abstractmethod

from util.numpy_dataset import NumpyDataset


class INumpyDatasetMixer(ABC):
    @abstractmethod
    def mix(self) -> NumpyDataset:
        """Creates NumpyDataset"""
