import logging

import matplotlib.pyplot as plt
import torch.utils.data

from config import current_config
from sp500_data import SP500DataModule
from util.model import ModelRepository
from vae.model import Vae
from vae.trainer import VaeTrainer

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)

if __name__ == "__main__":
    try:
        logging.info("main started")

        logging.info(f"cuda available: {torch.cuda.is_available()}")

        plt.close("all")

        cuda = torch.cuda.is_available()
        device = torch.device("cuda" if cuda else "cpu")

        data_module = SP500DataModule()
        data_module.prepare_data()
        data_module.setup()

        train_loader = data_module.train_dataloader()
        validate_loader = data_module.val_dataloader()

        model = Vae(current_config.n_features)

        trainer = VaeTrainer(model=model,
                             train_loader=train_loader,
                             validate_loader=validate_loader,
                             device=device)

        for epoch in range(1, 10):
            trainer.train(epoch)
            trainer.validate(epoch)

        ModelRepository.save_model_with_name_and_timestamp(model=model,
                                                           name="vae",
                                                           path=current_config.models_path.joinpath("vae"))


    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")
