from typing import List

from torch import device
from torch.utils.data import DataLoader

from experiment import Experiment
from two_dataset_mixer import TwoDatasetMixer
from util.data_loader import DataLoaderRepository
from util.numpy_dataset import NumpyDataset


def create_mixed_experiments(first_dataset: NumpyDataset, second_dataset: NumpyDataset,
                             validation_dataloader: DataLoader, device: device, batch_size: int) -> List[Experiment]:
    shuffle = False
    experiments: List[Experiment] = []
    for count_original in range(18):
        count_original = (count_original + 1) * 100
        count_generated = 2000 - count_original

        mixer = TwoDatasetMixer(first_dataset=first_dataset,
                                first_count=count_original,
                                second_dataset=second_dataset,
                                second_count=count_generated,
                                shuffle=shuffle)
        mixed_dataset = mixer.mix()
        mixed_dataloader = DataLoaderRepository.from_numpy_dataset(mixed_dataset,
                                                                   device=device,
                                                                   batch_size=batch_size)
        vae_experiment = Experiment(
            name=f"vae_mixed_original{count_original}_generated{count_generated}_shuffle{shuffle}",
            train_dataloader=mixed_dataloader,
            validation_dataloader=validation_dataloader)
        experiments.append(vae_experiment)
    return experiments
