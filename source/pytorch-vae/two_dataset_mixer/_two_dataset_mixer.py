from typing import Tuple

import numpy as np

from numpy_dataset_mixer import INumpyDatasetMixer
from util.numpy_dataset import NumpyDataset, NumpyDatasetRepository


class TwoDatasetMixer(INumpyDatasetMixer):
    _first_dataset: NumpyDataset
    _first_count: int

    _second_dataset: NumpyDataset
    _second_count: int

    _counts: Tuple[int, int]
    _shuffle: bool

    def __init__(self, first_dataset: NumpyDataset, first_count: int, second_dataset: NumpyDataset, second_count: int,
                 shuffle: bool = True):
        self._first_dataset = first_dataset
        self._first_count = first_count

        self._second_dataset = second_dataset
        self._second_count = second_count
        self._shuffle = shuffle

    @staticmethod
    def _shuffle_numpy_dataset(dataset: NumpyDataset) -> NumpyDataset:
        features_and_labels = np.hstack((dataset.features, dataset.labels))
        np.random.shuffle(features_and_labels)
        features = features_and_labels[:, :dataset.features.shape[1]]
        labels = features_and_labels[:, dataset.features.shape[1]:]
        return NumpyDataset(features, labels)

    @staticmethod
    def _select_n_observations(dataset: NumpyDataset, n_rows: int) -> NumpyDataset:
        random_row_indexes = np.random.randint(dataset.features.shape[0], size=n_rows)

        random_features = dataset.features[random_row_indexes, :]
        random_labels = dataset.labels[random_row_indexes, :]

        return NumpyDataset(features=random_features,
                            labels=random_labels)

    def mix(self) -> NumpyDataset:
        random_first_dataset = TwoDatasetMixer._select_n_observations(dataset=self._first_dataset,
                                                                      n_rows=self._first_count)
        random_second_dataset = TwoDatasetMixer._select_n_observations(dataset=self._second_dataset,
                                                                       n_rows=self._second_count)

        combined_dataset = NumpyDatasetRepository.add_two_datasets(random_first_dataset, random_second_dataset)

        if self._shuffle:
            combined_dataset = TwoDatasetMixer._shuffle_numpy_dataset(combined_dataset)

        return combined_dataset
