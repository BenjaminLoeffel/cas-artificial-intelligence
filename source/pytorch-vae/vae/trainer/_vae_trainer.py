import logging

import torch
import torch.utils.data
from torch import optim
from torch.nn import functional as F
from torch.utils.data import DataLoader

from vae.model import Vae


class VaeTrainer:

    def __init__(self, train_loader: DataLoader, validate_loader: DataLoader, model: Vae, device):
        self.train_loader = train_loader
        self._train_n_observations = len(self.train_loader.dataset)
        self._train_n_batches = len(self.train_loader)

        self.validate_loader = validate_loader

        self.device = device

        self.model = model.to(self.device)
        self.optimizer = optim.Adam(self.model.parameters(), lr=1e-3)

    def train(self, epoch: int):
        self.model.train()
        train_loss = 0
        for batch_idx, data in enumerate(self.train_loader):
            batch_size = len(data)
            data = data[0].to(self.device)
            self.optimizer.zero_grad()
            recon_batch, mu, logvar = self.model(data)
            loss = self.loss_function(recon_batch, data, mu, logvar)
            loss.backward()
            train_loss += loss.item()
            self.optimizer.step()
            if batch_idx % 10 == 0:
                logging.info(
                    f"Train Epoch: {epoch} [{batch_idx * batch_size}/{self._train_n_observations} ({100. * batch_idx / self._train_n_batches:.0f}%)]\tLoss: {loss.item() / batch_size:.6f}")

        logging.info(f"====> Epoch: {epoch} Average loss: {train_loss / self._train_n_observations:.4f}")

    def validate(self, epoch: int):
        self.model.eval()
        test_loss = 0
        with torch.no_grad():
            for i, data in enumerate(self.validate_loader):
                data = data[0].to(self.device)
                recon_batch, mu, logvar = self.model(data)
                test_loss += self.loss_function(recon_batch, data, mu, logvar).item()

        test_loss /= len(self.validate_loader.dataset)
        logging.info(f"====> Test set loss: {test_loss:.4f}")

        # Reconstruction + KL divergence losses summed over all elements and batch

    def loss_function(self, recon_x, x, mu, logvar):
        MSE = F.mse_loss(recon_x, x.view(-1, self.model.n_features), reduction='mean')
        # see Appendix B from VAE paper:
        # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        # https://arxiv.org/abs/1312.6114
        # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

        return MSE + KLD
