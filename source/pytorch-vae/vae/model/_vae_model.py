from __future__ import print_function

import numpy as np
import torch
import torch.utils.data
from torch import nn
from torch.nn import functional as F


class Vae(nn.Module):
    def __init__(self, n_features):
        super().__init__()
        self.n_features = n_features

        self.fc1 = nn.Linear(n_features, 400)
        self.fc21 = nn.Linear(400, 20)  # mu 30
        self.fc22 = nn.Linear(400, 20)  # log 30 variance
        self.fc3 = nn.Linear(20, 400)
        self.fc4 = nn.Linear(400, n_features)

    def encode(self, x):
        h1 = F.relu(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)

        return mu + eps * std

    def decode(self, z):
        h3 = F.relu(self.fc3(z))
        return self.fc4(h3)

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, x.shape[1]))
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar


class Predictor:

    def __init__(self, model: nn.Module, device):
        self.model = model
        self.device = device

    def predict(self, features: torch.Tensor) -> np.ndarray:
        with torch.no_grad():
            features = features.to(self.device)
            output = self.model(features)
            y_hat = output[2]
            y_hat = y_hat.cpu()
            return y_hat.numpy()
