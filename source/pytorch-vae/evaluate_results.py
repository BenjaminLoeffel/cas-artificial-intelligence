import json
import logging

import pandas as pd

from config import current_config

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)

if __name__ == "__main__":
    try:
        logging.info("main started")

        results_path = current_config.result_path

        result_files = results_path.glob("*.json")

        results = []

        for result_file in result_files:
            with open(result_file, "r") as f:
                data = json.load(f)[0]
                results.append({"name": result_file.stem,
                                "test_loss": data["test_loss"],
                                "test_accuracy": data["test_accuracy"]})
        results = pd.DataFrame(results)
        results.to_excel(current_config.result_path.joinpath("results.xlsx"))


    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")
