import logging
from pathlib import Path

import numpy as np


class NumpyDataset:
    _features: np.ndarray
    _labels: np.ndarray

    def __init__(self, features: np.ndarray, labels: np.ndarray):
        self._features = features
        self._labels = labels

    @property
    def features(self) -> np.ndarray:
        return self._features

    @property
    def labels(self) -> np.ndarray:
        return self._labels


class NumpyDatasetRepository:

    @staticmethod
    def _save_numpy_array_to_path(path: Path, array: np.ndarray):
        path.parent.mkdir(parents=True, exist_ok=True)
        np.save(path, array)
        logging.info(f"successfully saved to {path}")

    @staticmethod
    def save_to_path(dataset: NumpyDataset, path: Path, name: str):
        features_path = path.joinpath(f"{name}_features.npy")
        NumpyDatasetRepository._save_numpy_array_to_path(features_path, dataset.features)

        labels_path = path.joinpath(f"{name}_labels.npy")
        NumpyDatasetRepository._save_numpy_array_to_path(labels_path, dataset.labels)

    @staticmethod
    def load_from_path(path: Path, name: str) -> NumpyDataset:
        features_path = path.joinpath(f"{name}_features.npy")
        features = np.load(features_path)
        labels_path = path.joinpath(f"{name}_labels.npy")
        labels = np.load(labels_path)
        return NumpyDataset(features=features, labels=labels)

    @staticmethod
    def add_two_datasets(first_dataset: NumpyDataset, second_dataset: NumpyDataset) -> NumpyDataset:
        features = np.vstack((first_dataset.features, second_dataset.features))
        labels = np.vstack((first_dataset.labels, second_dataset.labels))
        return NumpyDataset(features, labels)
