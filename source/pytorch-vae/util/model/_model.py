import datetime
import logging
from pathlib import Path

import torch
from torch.nn import Module


class ModelRepository:

    @staticmethod
    def save(model: Module, path: Path):
        path.mkdir(parents=True, exist_ok=True)
        torch.save(model.state_dict(), path)
        logging.info(f"successfully saved model to {path}")

    @staticmethod
    def save_model_with_name_and_timestamp(model: Module, path: Path, name: str):
        filepath = path.joinpath(f"{name}_{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}_state_dict")
        filepath.parent.mkdir(parents=True, exist_ok=True)
        torch.save(model.state_dict(), filepath)
        logging.info(f"successfully saved model to {filepath}")
