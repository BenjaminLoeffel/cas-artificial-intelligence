import numpy as np
from torch import Tensor
from torch import device
from torch.utils.data import DataLoader, Dataset, TensorDataset

from util.numpy_dataset import NumpyDataset


class DataLoaderRepository:
    @staticmethod
    def get_features_as_tensor(data_loader: DataLoader) -> Tensor:
        return data_loader.dataset.tensors[0]

    @staticmethod
    def get_features_as_numpy(data_loader: DataLoader) -> np.ndarray:
        return DataLoaderRepository.get_features_as_tensor(data_loader).detach().numpy()

    @staticmethod
    def get_labels_as_tensor(data_loader: DataLoader) -> Tensor:
        return data_loader.dataset.tensors[1]

    @staticmethod
    def get_labels_as_numpy(data_loader: DataLoader) -> np.ndarray:
        return DataLoaderRepository.get_labels_as_tensor(data_loader).detach().numpy()

    @staticmethod
    def get_dataset(data_loader: DataLoader) -> Dataset:
        return data_loader.dataset

    @staticmethod
    def to_numpy_dataset(data_loader: DataLoader) -> NumpyDataset:
        return NumpyDataset(features=DataLoaderRepository.get_features_as_numpy(data_loader),
                            labels=DataLoaderRepository.get_labels_as_numpy(data_loader))

    @staticmethod
    def from_numpy_dataset(dataset: NumpyDataset, device: device, batch_size: int, shuffle: bool = True) -> DataLoader:
        features = Tensor(dataset.features)
        features.to(device)
        labels = Tensor(dataset.labels)
        labels.to(device)
        dataset = TensorDataset(features, labels)
        return DataLoader(dataset=dataset, batch_size=batch_size, shuffle=shuffle)
