import copy

import numpy as np

from numpy_dataset_mixer import INumpyDatasetMixer
from util.numpy_dataset import NumpyDataset


class InterlevaedDatasetMixer(INumpyDatasetMixer):
    _first_dataset: NumpyDataset
    _first_count: int

    _second_dataset: NumpyDataset
    _second_count: int

    _total_count: int

    def __init__(self, first_dataset: NumpyDataset, first_count: int, second_dataset: NumpyDataset, second_count: int,
                 total_count: int):
        self._first_dataset = first_dataset
        self._first_count = first_count

        self._second_dataset = second_dataset
        self._second_count = second_count
        self._total_count = total_count

    def mix(self) -> NumpyDataset:
        # there might be a more efficient interweaving: https://stackoverflow.com/questions/5347065/interweaving-two-numpy-arrays

        first_features_list = copy.deepcopy(self._first_dataset.features.tolist())
        first_labels_list = copy.deepcopy(self._first_dataset.labels.tolist())

        second_features_list = copy.deepcopy(self._second_dataset.features.tolist())
        second_labels_list = copy.deepcopy(self._second_dataset.labels.tolist())

        features = []
        labels = []

        while len(features) <= self._total_count:

            for first_count in range(self._first_count):
                features.append(first_features_list.pop(0))
                labels.append(first_labels_list.pop(0))

                second_features_list.pop(0)
                second_labels_list.pop(0)

            for second_count in range(self._second_count):
                first_features_list.pop(0)
                first_labels_list.pop(0)

                features.append(second_features_list.pop(0))
                labels.append(second_labels_list.pop(0))

        features = np.array(features)
        labels = np.array(labels)

        return NumpyDataset(features, labels)
