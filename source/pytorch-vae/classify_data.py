import datetime
import logging

import torch.utils.data
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger

from classifier import LitBinClassifier
from config import current_config
from create_interleaved_experiments import experiment_setups, create_interleaved_experiments
from create_mixed_experiments import create_mixed_experiments
from experiment import Experiment
from result import ResultRepository
from util.data_loader import DataLoaderRepository
from util.model import ModelRepository
from util.numpy_dataset import NumpyDatasetRepository

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)

if __name__ == "__main__":
    try:
        logging.info("main started")

        logging.info(f"cuda available: {torch.cuda.is_available()}")

        cuda = torch.cuda.is_available()
        device = torch.device("cuda" if cuda else "cpu")

        learning_rate = 0.0001
        batch_size = 16
        n_epochs = 25

        # train data (original)
        train_dataset = NumpyDatasetRepository.load_from_path(path=current_config.data_path,
                                                              name="train")
        train_dataloader = DataLoaderRepository.from_numpy_dataset(
            dataset=train_dataset,
            device=device,
            batch_size=batch_size)

        # validate data
        validate_dataset = NumpyDatasetRepository.load_from_path(path=current_config.data_path,
                                                                 name="validate")
        validation_dataloader = DataLoaderRepository.from_numpy_dataset(
            dataset=validate_dataset,
            batch_size=batch_size,
            device=device)

        # test data
        test_dataset = NumpyDatasetRepository.load_from_path(path=current_config.data_path,
                                                             name="test")
        test_dataloader = DataLoaderRepository.from_numpy_dataset(
            dataset=test_dataset,
            device=device,
            batch_size=batch_size)

        # generated data
        generated_dataset = NumpyDatasetRepository.load_from_path(path=current_config.data_path,
                                                                  name="generated")

        if True == False:
            experiments = [Experiment(name="baseline",
                                      train_dataloader=train_dataloader,
                                      validation_dataloader=validation_dataloader)]

            experiments = create_mixed_experiments(first_dataset=train_dataset,
                                                   second_dataset=generated_dataset,
                                                   validation_dataloader=validation_dataloader,
                                                   device=device,
                                                   batch_size=batch_size)

        experiments = create_interleaved_experiments(setups=experiment_setups, first_dataset=train_dataset,
                                                     second_dataset=generated_dataset,
                                                     validation_dataloader=validation_dataloader, device=device,
                                                     batch_size=batch_size)

        for experiment in experiments:
            classifier = LitBinClassifier()

            model_path = current_config.models_path.joinpath(experiment.name).joinpath(
                datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))

            tensorboard_logger = TensorBoardLogger(save_dir=current_config.tensorboard_path, name=experiment.name)

            trainer = Trainer(accelerator="auto",
                              devices=1,
                              log_every_n_steps=16,
                              auto_lr_find=False,
                              max_epochs=n_epochs,
                              logger=tensorboard_logger,
                              # callbacks=[early_stop_callback, checkpoint_callback]
                              )

            trainer.fit(model=classifier,
                        train_dataloaders=experiment.train_dataloader,
                        val_dataloaders=experiment.validation_dataloader)

            test_result = trainer.test(model=classifier,
                                       dataloaders=test_dataloader,
                                       verbose=True)

            ResultRepository.save_test_result(path=current_config.result_path,
                                              result=test_result,
                                              name=experiment.name)

            ModelRepository.save_model_with_name_and_timestamp(model=classifier,
                                                               path=current_config.models_path.joinpath("classifier"),
                                                               name=experiment.name)

    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")
