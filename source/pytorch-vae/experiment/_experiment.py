from torch.utils.data import DataLoader


class Experiment:

    def __init__(self, name: str, train_dataloader: DataLoader, validation_dataloader: DataLoader):
        self.name = name
        self.train_dataloader = train_dataloader
        self.validation_dataloader = validation_dataloader
