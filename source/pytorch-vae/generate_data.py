import logging

import numpy as np
# import torch.utils.data
import torch

from config import current_config
from sp500_data import SP500DataModule
from util.data_loader import DataLoaderRepository
from util.numpy_dataset import NumpyDatasetRepository, NumpyDataset
# from util.dataset import DatasetRepository
from vae.model import Vae

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])

if __name__ == "__main__":
    try:
        logging.info("main started")

        logging.info(f"cuda available: {torch.cuda.is_available()}")

        cuda = torch.cuda.is_available()
        device = torch.device("cuda" if cuda else "cpu")

        data_module = SP500DataModule()
        data_module.prepare_data()
        data_module.setup()

        train_loader = data_module.train_dataloader()
        validate_loader = data_module.val_dataloader()
        test_loader = data_module.test_dataloader()

        model = Vae(current_config.n_features)
        saved_model = current_config.models_path.joinpath("vae").joinpath("vae_20211127110848_state_dict")
        model.load_state_dict(torch.load(saved_model))

        samples = []
        generated_features = []
        generated_labels = []
        for i in range(1):
            with torch.no_grad():
                features = DataLoaderRepository.get_features_as_tensor(train_loader)
                labels = DataLoaderRepository.get_labels_as_tensor(train_loader)

                predictions, _, _ = model(features)

                new_features = predictions.numpy()  # VAE is used to create "new" artificial data
                new_labels = labels.numpy()  # labels are taken as a copy of the already existing labels

                generated_features.append(new_features)
                generated_labels.append(new_labels)

        train_dataset = DataLoaderRepository.to_numpy_dataset(train_loader)

        validate_dataset = DataLoaderRepository.to_numpy_dataset(validate_loader)

        test_dataset = DataLoaderRepository.to_numpy_dataset(test_loader)

        generated_features = np.vstack(generated_features)
        generated_labels = np.vstack(generated_labels)

        NumpyDatasetRepository.save_to_path(dataset=train_dataset,
                                            path=current_config.data_path,
                                            name="train")

        NumpyDatasetRepository.save_to_path(dataset=validate_dataset,
                                            path=current_config.data_path,
                                            name="validate")

        NumpyDatasetRepository.save_to_path(dataset=test_dataset,
                                            path=current_config.data_path,
                                            name="test")

        NumpyDatasetRepository.save_to_path(dataset=NumpyDataset(features=generated_features,
                                                                 labels=generated_labels),
                                            path=current_config.data_path,
                                            name="generated")


    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")
