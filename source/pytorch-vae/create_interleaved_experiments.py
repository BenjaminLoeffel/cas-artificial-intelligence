import logging
from typing import List

from torch import device
from torch.utils.data import DataLoader

from experiment import Experiment
from interlaved_dataset_mixer import InterlevaedDatasetMixer
from util.data_loader import DataLoaderRepository
from util.numpy_dataset import NumpyDataset


class InterleavedMixerExperiment:
    _first_count: int
    _second_count: int
    _total_count: int

    def __init__(self, first_count: int, second_count: int, total_count: int):
        self._first_count = first_count
        self._second_count = second_count
        self._total_count = total_count

    @property
    def first_count(self) -> int:
        return self._first_count

    @property
    def second_count(self) -> int:
        return self._second_count

    @property
    def total_count(self) -> int:
        return self._total_count


def create_interleaved_experiments(setups: List[InterleavedMixerExperiment], first_dataset: NumpyDataset,
                                   second_dataset: NumpyDataset, validation_dataloader: DataLoader, device: device,
                                   batch_size: int) -> List[Experiment]:
    experiments: List[Experiment] = []
    for setup in setups:
        mixer = InterlevaedDatasetMixer(first_dataset=first_dataset, first_count=setup.first_count,
                                        second_dataset=second_dataset, second_count=setup.second_count,
                                        total_count=setup.total_count)
        mixed_dataset = mixer.mix()
        mixed_dataloader = DataLoaderRepository.from_numpy_dataset(mixed_dataset,
                                                                   device=device,
                                                                   batch_size=batch_size)
        experiment = Experiment(
            name=f"vae_interleaved_original{setup.first_count}_generated{setup.second_count}",
            train_dataloader=mixed_dataloader,
            validation_dataloader=validation_dataloader)
        experiments.append(experiment)
        logging.info(f"created experiment {experiment.name}")
    return experiments


experiment_setups = [InterleavedMixerExperiment(first_count=1, second_count=1, total_count=2000),
                     InterleavedMixerExperiment(first_count=2, second_count=2, total_count=2000),
                     InterleavedMixerExperiment(first_count=1, second_count=3, total_count=2000),
                     InterleavedMixerExperiment(first_count=1, second_count=4, total_count=2000),
                     InterleavedMixerExperiment(first_count=5, second_count=5, total_count=2000),
                     InterleavedMixerExperiment(first_count=10, second_count=10, total_count=2000)]
