from pathlib import Path

import torchvision.datasets

data_path = Path(__file__).parent

dataset = torchvision.datasets.EMNIST(root=data_path,
                                      split="byclass",
                                      train=True,
                                      download=True)

a = 10
