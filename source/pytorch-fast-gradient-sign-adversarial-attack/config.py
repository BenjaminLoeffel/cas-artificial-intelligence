from pathlib import Path

import numpy as np
import torch


class Config:
    _tensorboard_path: Path
    _models_path: Path
    _data_path: Path
    _result_path: Path
    _n_features: int

    def __init__(self, tensorboard_path: Path, models_path: Path, data_path: Path, result_path: Path, n_features: int):
        self._tensorboard_path = tensorboard_path
        self._models_path = models_path
        self._data_path = data_path
        self._result_path = result_path
        self._n_features = n_features

    @property
    def tensorboard_path(self) -> Path:
        return self._tensorboard_path

    @property
    def models_path(self) -> Path:
        return self._models_path

    @property
    def data_path(self) -> Path:
        return self._data_path

    @property
    def result_path(self) -> Path:
        return self._result_path

    @property
    def n_features(self) -> int:
        return self._n_features


torch.manual_seed(0)
np.random.seed(0)

current_config = Config(tensorboard_path=Path.cwd().joinpath("tensorboard_logs"),
                        models_path=Path.cwd().joinpath("saved_models"),
                        data_path=Path.cwd().joinpath("data"),
                        result_path=Path.cwd().joinpath("results"),
                        n_features=489)
