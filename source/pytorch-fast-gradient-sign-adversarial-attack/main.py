import logging

import torch.utils.data

from dataset import dataset

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)

if __name__ == "__main__":
    try:
        logging.info("main started")

        logging.info(f"cuda available: {torch.cuda.is_available()}")

        a = dataset



    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")
