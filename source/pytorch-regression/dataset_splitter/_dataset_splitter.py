from typing import Tuple

import torch.utils.data
from torch.utils.data import Dataset, Subset

from dataset import RegressionDataset


class DatasetSplitter:
    _batch_size: int
    _train_ratio: float
    _validation_ratio: float

    def __init__(self, batch_size: int = 65, train_ratio: float = 0.8):
        self._batch_size = batch_size
        self._train_ratio = train_ratio
        self._validation_ratio = 1 - self._train_ratio

    @staticmethod
    def _create_dataset_from_subset(subset: Subset) -> RegressionDataset:
        features, labels = subset.dataset[subset.indices]
        return RegressionDataset(features=features, labels=labels)

    def split(self, dataset: Dataset) -> Tuple[RegressionDataset, RegressionDataset]:
        dataset_length = len(dataset)
        train_size = int(self._train_ratio * dataset_length)
        test_size = dataset_length - train_size
        train_subset, test_subset = torch.utils.data.random_split(dataset, [train_size, test_size])
        train_dataset = DatasetSplitter._create_dataset_from_subset(train_subset)
        test_dataset = DatasetSplitter._create_dataset_from_subset(test_subset)
        return train_dataset, test_dataset
