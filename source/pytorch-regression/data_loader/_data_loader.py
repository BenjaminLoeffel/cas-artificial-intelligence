from typing import Tuple

from torch.utils.data import Dataset, DataLoader


class RegressionDataLoader:
    _batch_size: int

    def __init__(self, batch_size: int):
        self._batch_size = batch_size

    def load(self, train_dataset: Dataset, test_dataset: Dataset) -> Tuple[DataLoader, DataLoader]:
        return DataLoader(train_dataset, batch_size=self._batch_size), DataLoader(test_dataset,
                                                                                  batch_size=self._batch_size)
