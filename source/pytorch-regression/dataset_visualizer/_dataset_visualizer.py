import matplotlib.pyplot as plt

from dataset import RegressionDataset


class DatasetVisualizer:

    @staticmethod
    def visualize(ax: plt.Axes, dataset: RegressionDataset, label: str) -> plt.Axes:
        ax.scatter(dataset.features, dataset.labels, label=label)
        return ax
