class Loss:
    _epoch: int
    _loss: float

    def __init__(self, epoch: int, loss: float):
        self._epoch = epoch
        self._loss = loss

    @property
    def epoch(self) -> int:
        return self._epoch

    @property
    def loss(self) -> float:
        return self._loss
