import numpy as np

from dataset import RegressionDataset


class DatasetGenerator:

    @staticmethod
    def model_function(x: np.ndarray) -> np.ndarray:
        return x ** 3

    @staticmethod
    def apply_noise(x: np.ndarray, mu: float, sigma: float) -> np.ndarray:
        return x + sigma * np.random.randn(len(x)) + mu

    @staticmethod
    def create_dataset(x: np.ndarray) -> RegressionDataset:
        y = DatasetGenerator.model_function(x)
        y = DatasetGenerator.apply_noise(y, mu=0, sigma=50)
        dataset = RegressionDataset(features=x, labels=y)
        return dataset
