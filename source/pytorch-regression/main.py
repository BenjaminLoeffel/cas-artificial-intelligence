import logging

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn

from data_loader import RegressionDataLoader
from dataset_generator import DatasetGenerator
from dataset_splitter import DatasetSplitter
from dataset_visualizer import DatasetVisualizer
from model import NeuralNetwork, Predictor
from trainer import Trainer
from training_visualizer import TrainingVisualizer

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)


def main():
    try:
        logging.info("main started")

        logging.info(f"cuda available: {torch.cuda.is_available()}")

        plt.close("all")

        x = np.linspace(-10, 10, 1000)

        dataset = DatasetGenerator.create_dataset(x=x)

        dataset_splitter = DatasetSplitter()
        train_dataset, test_dataset = dataset_splitter.split(dataset)

        figure = plt.figure()

        ax_0 = figure.add_subplot(2, 1, 1)
        ax_0.set_title(f"complete dataset, n_samples: {len(dataset)}")
        DatasetVisualizer.visualize(ax_0, dataset, label="complete dataset")

        ax_1 = figure.add_subplot(2, 1, 2)
        ax_1.set_title(f"train dataset, n_samples: {len(train_dataset)}")
        DatasetVisualizer.visualize(ax_1, train_dataset, label="train dataset")
        #
        # ax_2 = figure.add_subplot(3, 1, 3)
        # ax_2.set_title(f"test dataset, n_samples: {len(test_dataset)}")
        # DatasetVisualizer.visualize(ax_2, train_dataset)

        learning_rate = 0.05
        batch_size = 50
        n_epochs = 100

        regression_loader = RegressionDataLoader(batch_size=batch_size)

        train_dataloader, test_dataloader = regression_loader.load(train_dataset, test_dataset)

        device = "cuda" if torch.cuda.is_available() else "cpu"

        model = NeuralNetwork(input_size=1).double().to(device)
        loss_function = torch.nn.MSELoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
        logging.info(model)

        trainer = Trainer(train_dataloader=train_dataloader,
                          test_data_loader=test_dataloader,
                          model=model,
                          loss_function=loss_function,
                          optimizer=optimizer,
                          device=device,
                          n_epochs=n_epochs)

        trainer.train()

        figure, ax = TrainingVisualizer.get_ax()
        TrainingVisualizer.visualize_losses(ax=ax, losses=trainer.losses)

        predictor = Predictor(model=model, device=device)

        x_validation = np.linspace(-15, 15, 100)
        y_validation = predictor.predict(x=x_validation)

        ax_1.scatter(x_validation, y_validation, label="validation", color="red")
        ax_1.legend()

        plt.show()

    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")


if __name__ == "__main__":
    main()
