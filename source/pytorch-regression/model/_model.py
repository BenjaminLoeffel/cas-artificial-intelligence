import numpy as np
import torch
from torch import nn


class NeuralNetwork(nn.Module):
    _input_size: int

    def __init__(self, input_size: int):
        super().__init__()
        self._input_size = input_size
        self.linear_relu_stack = nn.Sequential(nn.Linear(1, 20),
                                               nn.ReLU(),
                                               nn.Linear(20, 10),
                                               nn.ReLU(),
                                               nn.Linear(10, 1))

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits


class Predictor:
    _model: nn.Module
    _device: str

    def __init__(self, model: nn.Module, device: str):
        self._model = model
        self._device = device

    def predict(self, x: np.ndarray) -> np.ndarray:
        x = x.transpose()
        with torch.no_grad():
            x = torch.from_numpy(x).unsqueeze(1).to(self._device)
            y_pred = self._model(x)
            y_pred = y_pred.cpu()
            return y_pred.numpy()
