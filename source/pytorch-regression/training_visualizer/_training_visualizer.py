from typing import List, Tuple

import matplotlib.pyplot as plt

from loss import Loss


class TrainingVisualizer:

    @staticmethod
    def get_ax() -> Tuple[plt.Figure, plt.Axes]:
        figure = plt.figure()
        ax = figure.add_subplot(1, 1, 1)
        ax.set_title("Loss")
        ax.set_xlabel("epoch / 1")
        ax.set_ylabel("loss / 1")
        return figure, ax

    @staticmethod
    def visualize_losses(ax: plt.Axes, losses: List[Loss]):
        epochs = []
        loss_values = []
        for loss in losses:
            epochs.append(loss.epoch)
            loss_values.append(loss.loss)
        ax.plot(epochs, loss_values)
