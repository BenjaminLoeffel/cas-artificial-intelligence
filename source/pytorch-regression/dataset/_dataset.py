from typing import Tuple

import numpy as np
from torch.utils.data import Dataset


class RegressionDataset(Dataset):
    _features: np.ndarray
    _labels: np.ndarray

    def __init__(self, features: np.ndarray, labels: np.ndarray):
        self._features = features
        self._labels = labels

    def __len__(self):
        return len(self._features)

    def __getitem__(self, index: int) -> Tuple[float, float]:
        return self._features[index], self._labels[index]

    @property
    def features(self) -> np.ndarray:
        return self._features

    @property
    def labels(self) -> np.ndarray:
        return self._labels
