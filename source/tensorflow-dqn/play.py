import datetime
import logging

import gym
import numpy as np
import torch
import torch.nn

from config import current_config
from logging_util.builder import LoggingBuilder
from model_repository import ModelRepository
from torch_model import TorchModel

_logger = logging.getLogger()
builder = LoggingBuilder(_logger)
builder.add_default_loggers(__file__)


def main():
    try:
        _logger.info("main started")

        _logger.info(f"cuda available: {torch.cuda.is_available()}")

        experiment_name = f"experiment_{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}"
        experiment_name = "current"

        torch_device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        torch_device = torch.device("cpu")

        env = gym.make("CartPole-v1")

        n_states = env.env.observation_space.shape[0]
        n_actions = env.env.action_space.n

        model = TorchModel(n_states=n_states, n_actions=n_actions)

        model_repository = ModelRepository(basepath=current_config.models_path.joinpath(experiment_name))

        model.load_state_dict(model_repository.load_state_dict("model"))

        for episode in range(10):
            total_reward = 0.0
            state = env.reset()
            state = np.reshape(state, newshape=(1, -1)).astype(np.float32)

            while True:
                env.render()
                action = np.argmax(model(state).detach().numpy())
                next_state, reward, done, _ = env.step(action)
                next_state = np.reshape(next_state, newshape=(1, -1)).astype(np.float32)
                total_reward += reward
                state = next_state

                if done:
                    print(f"Episode: {episode} Reward: {total_reward}")
                    break


    except Exception as e:
        _logger.exception(f"exception occurred {e}", exc_info=True)

    finally:
        _logger.info("main finished")


if __name__ == "__main__":
    main()
