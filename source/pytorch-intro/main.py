import logging
import torch

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger('matplotlib').setLevel(logging.ERROR)


def main():
    try:

        logging.info("main started")

        logging.info(f"cuda avaliable: {torch.cuda.is_available()}")

    except Exception as e:
        logging.exception("exception occurred", exc_info=True)

    finally:
        logging.info("main finished")


if __name__ == "__main__":
    main()
