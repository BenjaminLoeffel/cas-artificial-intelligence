import datetime
import logging
from typing import List, Union

import numpy as np
import torch
from sklearn.metrics import mean_absolute_percentage_error
from torch.nn import MSELoss, BCELoss
from torch.nn import Module
from torch.optim import Optimizer
from torch.utils.data import DataLoader

from loss import Loss


class Trainer:
    _train_dataloader: DataLoader
    _test_data_loader: DataLoader
    _model: Module
    _loss_function: Union[MSELoss, BCELoss]
    _optimizer: Optimizer
    _device: str
    _n_epochs: int
    _losses: List[Loss]
    _start_time: datetime.datetime
    _stop_time: datetime.datetime

    def __init__(self, train_dataloader: DataLoader, test_data_loader: DataLoader, model: Module,
                 loss_function: Union[MSELoss, BCELoss], optimizer: Optimizer, device: str, n_epochs: int):
        self._train_dataloader = train_dataloader
        self._test_data_loader = test_data_loader
        self._model = model
        self._loss_function = loss_function
        self._optimizer = optimizer
        self._device = device
        self._n_epochs = n_epochs

        self._losses = []

    @property
    def losses(self) -> List[Loss]:
        return self._losses

    def _train(self, epoch: int):
        size = len(self._train_dataloader.dataset)
        self._model.train()
        for batch, (X, y) in enumerate(self._train_dataloader):
            X, y = X.unsqueeze(1).to(self._device), y.unsqueeze(1).to(self._device)

            # Compute prediction error
            pred = self._model(X)
            loss = self._loss_function(pred, y)

            # Backpropagation
            self._optimizer.zero_grad()
            loss.backward()
            self._optimizer.step()

            if batch % 10 == 0:
                loss, current = loss.item(), batch * len(X)
                logging.info(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

    def _test(self, epoch: int):
        mean_absolute_percentage_errors = []
        with torch.no_grad():
            for batch, (X, y) in enumerate(self._test_data_loader):
                X, y = X.unsqueeze(1).to(self._device), y.unsqueeze(1).to(self._device)
                y_pred = self._model(X)
                y_pred = y_pred.cpu().numpy()
                y_true = y.cpu().numpy()

                mean_absolute_percentage_errors.append(
                    mean_absolute_percentage_error(y_true=y_true, y_pred=y_pred))

        overall_absolute_percentage_error = np.mean(mean_absolute_percentage_errors)
        self._losses.append(Loss(epoch=epoch, loss=overall_absolute_percentage_error))

        logging.info(f"test, MAPE: {overall_absolute_percentage_error}")

    def train(self):
        logging.info(f"training started on device {self._device}")
        self._start_time = datetime.datetime.now()
        for epoch in range(self._n_epochs):
            logging.info(f"Epoch {epoch}")
            self._train(epoch=epoch)
            self._test(epoch=epoch)
        self._stop_time = datetime.datetime.now()
        logging.info(f"training finished on device {self._device}")
        logging.info(f"training took {self.training_duration} s")

    @property
    def training_duration(self) -> float:
        return (self._stop_time - self._start_time).total_seconds()
