import datetime
import logging
import os

import matplotlib.pyplot as plt
import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger

from config import current_config
from data_loader import RegressionDataLoader
from dataset_generator import DatasetGenerator
from dataset_splitter import DatasetSplitter
from dataset_visualizer import DatasetVisualizer
from model import NeuralNetwork, Predictor

logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler()])
logging.getLogger("matplotlib").setLevel(logging.ERROR)


def main():
    try:
        logging.info("main started")

        logging.info(f"cuda available: {torch.cuda.is_available()}")

        plt.close("all")
        pl.seed_everything(0)

        x = np.linspace(-10, 10, 1000)

        dataset = DatasetGenerator.create_dataset(x=x)

        dataset_splitter = DatasetSplitter()
        train_dataset, test_dataset = dataset_splitter.split(dataset)

        figure = plt.figure()

        ax_0 = figure.add_subplot(2, 1, 1)
        ax_0.set_title(f"complete dataset, n_samples: {len(dataset)}")
        DatasetVisualizer.visualize(ax_0, dataset, label="complete dataset")

        ax_1 = figure.add_subplot(2, 1, 2)
        ax_1.set_title(f"train dataset, n_samples: {len(train_dataset)}")
        DatasetVisualizer.visualize(ax_1, train_dataset, label="train dataset")

        learning_rate = 0.05
        batch_size = 50
        n_epochs = 100

        regression_loader = RegressionDataLoader(batch_size=batch_size)

        train_dataloader, validation_dataloader = regression_loader.load(train_dataset, test_dataset)

        model = NeuralNetwork(input_size=1, learning_rate=learning_rate)
        model.double()

        if not os.path.exists("models"):
            os.makedirs("models")

        early_stop_callback = EarlyStopping(monitor="val_accuracy", min_delta=0.1, patience=20, verbose=True,
                                            mode="min")
        model_path = current_config.models_path.joinpath(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))

        checkpoint_callback = ModelCheckpoint(monitor="val_accuracy",
                                              dirpath=model_path,
                                              filename="model-{epoch:02d}-{val_accuracy:.2f}",
                                              save_top_k=5,
                                              mode="max")

        tensorboard_logger = TensorBoardLogger(save_dir=current_config.tensorboard_path, name="my_model")

        trainer = Trainer(accelerator="auto",
                          devices=1,
                          log_every_n_steps=16,
                          auto_lr_find=False,
                          max_epochs=n_epochs,
                          logger=tensorboard_logger,
                          callbacks=[early_stop_callback, checkpoint_callback])

        # lr_finder = trainer.tuner.lr_find(model,
        #                                   train_dataloader,
        #                                   validation_dataloader,
        #                                   min_lr=1e-08,
        #                                   max_lr=1,
        #                                   num_training=300,
        #                                   mode="exponential",
        #                                   early_stop_threshold=8.0)
        #
        # # Plot with
        # fig = lr_finder.plot(suggest=True)
        # fig.show()
        #
        # # Pick point based on plot, or get suggestion
        # suggested_lr = lr_finder.suggestion(skip_begin=1, skip_end=10)
        #
        # logging.info(f"suggested learning rate {suggested_lr}")
        # # update hparams of the model
        # # model.hparams.lr = new_lr

        trainer.fit(model,
                    train_dataloaders=train_dataloader,
                    val_dataloaders=validation_dataloader)

        predictor = Predictor(model=model)

        x_validation = np.linspace(-10, 10, 100)
        y_validation = predictor.predict(x=x_validation)

        ax_1.scatter(x_validation, y_validation, label="test", color="red")
        ax_1.legend()

        plt.show()

    except Exception as e:
        logging.exception(f"exception occurred {e}", exc_info=True)

    finally:
        logging.info("main finished")


if __name__ == "__main__":
    main()
