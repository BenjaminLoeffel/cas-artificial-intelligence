from typing import List

import numpy as np
import pytorch_lightning as pl
import torch
import torchmetrics
from torch import Tensor
from torch import nn


class NeuralNetwork(pl.LightningModule):
    _input_size: int
    lr: float

    def __init__(self, input_size: int, learning_rate: float):
        super().__init__()
        self._loss_function = torch.nn.MSELoss()
        self._input_size = input_size
        self.lr = learning_rate
        self.metric = torchmetrics.MeanAbsolutePercentageError()
        self.metric.to(self.device)
        self.linear_relu_stack = nn.Sequential(nn.Linear(1, 20),
                                               nn.ReLU(),
                                               nn.Linear(20, 10),
                                               nn.ReLU(),
                                               nn.Linear(10, 1))

    def forward(self, x: Tensor):
        logits = self.linear_relu_stack(x)
        return logits

    def training_step(self, batch: List[Tensor], batch_idx: int):
        x, y = batch
        x = x.unsqueeze(1)
        y = y.unsqueeze(1)
        y_hat = self(x)
        loss = self._loss_function(y_hat, y)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss

    def validation_step(self, batch: List[Tensor], batch_idx: int):
        x, y = batch
        x = x.unsqueeze(1)
        y = y.unsqueeze(1)
        y_hat = self(x)
        acc = self.metric(y_hat, y)
        self.log("val_accuracy", acc, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return acc

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return optimizer


class Predictor:
    _model: pl.LightningModule

    def __init__(self, model: pl.LightningModule):
        self._model = model

    def predict(self, x: np.ndarray) -> np.ndarray:
        x = x.transpose()
        x = torch.from_numpy(x).unsqueeze(1).to(self._model.device)
        y_pred = self._model(x)
        y_pred = y_pred.cpu()
        return y_pred.detach().numpy()
