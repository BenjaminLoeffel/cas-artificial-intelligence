from abc import ABC, abstractmethod
from typing import Tuple, Any

import gym

from experience import Experience


class IAgent(ABC):

    @property
    @abstractmethod
    def state(self) -> Any:
        """Returns current state."""

    @property
    @abstractmethod
    def environment(self) -> gym.Env:
        """Returns gym.Env."""

    @abstractmethod
    def play_action(self, action: int) -> Tuple[float, bool, Experience]:
        """
        Carries out a single action on the environment.
        :param action:
        :return:
        """


class Agent(IAgent):
    _environment: gym.Env
    _state: Any

    def __init__(self, environment: gym.Env, initial_state: Any):
        """
        :param environment:
        :param initial_state:
        """
        self._environment = environment
        self._state = initial_state

    @property
    def state(self) -> Any:
        return self._state

    @property
    def environment(self) -> gym.Env:
        return self._environment

    def play_action(self, action: int) -> Tuple[float, bool, Experience]:
        # do step in the environment
        new_state, reward, done, _ = self._environment.step(action)
        experience = Experience(self._state, action, reward, done, new_state)
        self._state = new_state
        return reward, done, experience
