import logging
from pathlib import Path

import torch
from torch import nn

_logger = logging.getLogger(__file__)


class ModelRepository:

    def __init__(self, basepath: Path):
        self.basepath = basepath

    def save_model(self, model: nn.Module, filename: str):
        model_file = self.basepath.joinpath(f"{filename}.ckpt")
        model_file.parent.mkdir(parents=True, exist_ok=True)
        torch.save(model.state_dict(), f=model_file)
        _logger.info(f"saved model to {model_file}")

    def load_model(self, model: nn.Module, state_dict_name: str) -> nn.Module:
        state_dict = torch.load(self.basepath.joinpath(state_dict_name))
        model.load_state_dict(state_dict)
        return model
