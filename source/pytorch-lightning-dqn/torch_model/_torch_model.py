from typing import Any

from torch import nn

from model import IModel


class TorchModel(nn.Module, IModel):
    """Simple MLP network."""

    def __init__(self, n_observations: int, n_actions: int, hidden_size: int = 128):
        """
        Args:
            n_observations: observation/state size of the environment
            n_actions: number of discrete actions available in the environment
            hidden_size: size of hidden layers
        """
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(n_observations, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, n_actions))

    def forward(self, x: Any) -> Any:
        return self.net(x.float())
