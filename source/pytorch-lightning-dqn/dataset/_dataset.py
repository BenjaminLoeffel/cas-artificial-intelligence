from typing import Iterator

from torch.utils.data.dataset import IterableDataset

from replay_buffer import IReplayBuffer


class Dataset(IterableDataset):
    _buffer: IReplayBuffer
    _sample_size: int

    def __init__(self, buffer: IReplayBuffer, sample_size: int = 200) -> None:
        self._buffer = buffer
        self._sample_size = sample_size

    def __iter__(self) -> Iterator:
        states, actions, rewards, dones, new_states = self._buffer.sample(self._sample_size)
        for i in range(len(dones)):
            yield states[i], actions[i], rewards[i], dones[i], new_states[i]
