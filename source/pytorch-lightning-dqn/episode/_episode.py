import logging

import numpy as np
import torch
from torch import nn
from torch.optim import Optimizer
from torch.utils.tensorboard import SummaryWriter

from action_generator import RandomActionGenerator, ModelBasedActionGenerator, IActionGenerator
from agent import IAgent
from data_repository import IDataRepository
from epsilon_generator import IEpsilonGenerator
from model_repository import ModelRepository

_logger = logging.getLogger(__file__)


class Episode:
    _agent: IAgent
    _episode_number: int
    _data_repository: IDataRepository
    _optimizer: Optimizer
    _global_current_step: int
    _current_epoch: int
    _done: bool
    _steps_in_episode: int

    def __init__(self, agent: IAgent,
                 episode_number: int,
                 global_current_step: int,
                 data_repository: IDataRepository,
                 epsilon_generator: IEpsilonGenerator,
                 model: nn.Module,
                 target_model: nn.Module,
                 torch_device: torch.device,
                 optimizer: Optimizer,
                 summary_writer: SummaryWriter,
                 model_repository: ModelRepository,
                 gamma: float = 0.99,
                 sync_rate: int = 10):
        """
        :param agent: agent which plays environment
        :param model: model
        :param target_model: model that gets trained
        :param torch_device: torch device
        :param gamma:
        :param sync_rate: how often model and target_model are synced
        """
        self._agent = agent
        self._episode_number = episode_number
        self._data_repository = data_repository
        self._epsilon_generator = epsilon_generator

        self._model = model
        self._target_model = target_model
        self._torch_device = torch_device
        self._optimizer = optimizer
        self._gamma = gamma
        self._sync_rate = sync_rate

        self._total_episode_reward = 0
        self._global_current_step = global_current_step

        self._summary_writer: SummaryWriter = summary_writer
        self._model_repository: ModelRepository = model_repository

        self._steps_in_episode = 0
        self._done = False

    def run_episode(self):
        # let's play an episode
        while not self._done and self._steps_in_episode <= 450:
            # get epsilon based on global current step
            epsilon = self._epsilon_generator.get_epsilon(step=self._global_current_step)

            # get next action generator and action based on epsilon
            action_generator: IActionGenerator
            if np.random.random() < epsilon:
                action_generator = RandomActionGenerator(environment=self._agent.environment)

            else:
                action_generator = ModelBasedActionGenerator(torch_device=self._torch_device,
                                                             model=self._model,
                                                             state=self._agent.state)

            action = action_generator.get_next_action()

            # play action on environment and get reward, done and experience
            reward, done, experience = self._agent.play_action(action=action)

            # increment overall reward in episode with reward of executed step
            self._total_episode_reward += reward

            # log some metrics
            # self._summary_writer.add_scalar("reward/total", self._total_episode_reward, self._global_current_step)
            self._summary_writer.add_scalar("reward/current", reward, self._global_current_step)

            # get buffer and append new experience to it
            buffer = self._data_repository.get_replay_buffer()
            buffer.append(experience)

            # implement learn
            data_loader = self._data_repository.get_dataloader()

            for index, batch in enumerate(data_loader):
                states, actions, rewards, dones, next_states = batch

                state_action_values = self._model(states).gather(1, actions.type(torch.int64).unsqueeze(-1)).squeeze(
                    -1)

                with torch.no_grad():
                    next_state_values = self._target_model(next_states).max(1)[0]
                    next_state_values[dones] = 0.0
                    next_state_values = next_state_values.detach()

                expected_state_action_values = next_state_values * self._gamma + rewards
                loss_function = nn.MSELoss()
                loss = loss_function(state_action_values, expected_state_action_values)

                self._optimizer.zero_grad()
                loss.backward()
                self._optimizer.step()

                # Soft update of target network
                if self._global_current_step % self._sync_rate == 0:
                    self._target_model.load_state_dict(self._model.state_dict())
                    self._target_model.to(self._torch_device)
                    self._model.to(self._torch_device)

                self._summary_writer.add_scalar("loss", loss, self._global_current_step)

                # save the weights of model after each training
                if self._global_current_step % 100 == 0:
                    self._model_repository.save_model(model=self._model,
                                                      filename=f"episode_{self._episode_number}_step_{self._global_current_step}")

                # increment global_current_step
                self._global_current_step += 1
                self._steps_in_episode += 1

            # check if we are done
            if done:
                self._done = done

        # save the weights of model after each training
        self._model_repository.save_model(model=self._model,
                                          filename=f"episode_{self._episode_number}_{self._global_current_step}_end_of_episode")
