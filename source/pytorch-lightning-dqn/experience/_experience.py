import numpy as np


class Experience:
    state: np.ndarray
    action: int
    reward: float
    done: bool
    new_state: np.ndarray
    _iterator_index: int

    def __init__(self, state: np.ndarray, action: int, reward: float, done: bool, new_state: np.ndarray):
        self.state = state
        self.action = action
        self.reward = reward
        self.done = done
        self.new_state = new_state
        self._properties = [self.state, self.action, self.reward, self.done, self.new_state]

    def __iter__(self):
        self._iterator_index = 0
        return self

    def __next__(self):
        if self._iterator_index <= 4:
            property = self._properties[self._iterator_index]
            self._iterator_index += 1
            return property
        else:
            raise StopIteration
