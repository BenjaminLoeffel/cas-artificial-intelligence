import logging

import torch
from torch import nn
from torch.optim import Optimizer
from torch.utils.tensorboard import SummaryWriter

from action_generator_factory import IActionGeneratorFactory
from agent_factory import IAgentFactory
from data_repository import IDataRepository
from episode import Episode
from epsilon_generator import IEpsilonGenerator
from model_repository import ModelRepository

_logger = logging.getLogger(__file__)


class Trainer:
    _agent_factory: IAgentFactory
    _data_repository: IDataRepository
    _epsilon_generator: IEpsilonGenerator
    _optimizer: Optimizer
    _global_current_step: int
    _global_current_epoch: int
    _action_generator_factory: IActionGeneratorFactory

    def __init__(self,
                 agent_factory: IAgentFactory,
                 data_repository: IDataRepository,
                 epsilon_generator: IEpsilonGenerator,
                 model: nn.Module,
                 target_model: nn.Module,
                 torch_device: torch.device,
                 optimizer: Optimizer,
                 summary_writer: SummaryWriter,
                 n_episodes: int,
                 model_repository: ModelRepository,
                 gamma: float = 0.99,
                 sync_rate: int = 10):
        """
        :param agent: agent which plays environment
        :param model: model
        :param target_model: model that gets trained
        :param torch_device: torch device
        :param gamma:
        :param sync_rate: how often model and target_model are synced
        """
        self._agent_factory = agent_factory
        self._data_repository = data_repository
        self._epsilon_generator = epsilon_generator

        self._model = model
        self._target_model = target_model
        self._torch_device = torch_device
        self._optimizer = optimizer

        self._n_episodes = n_episodes
        self._gamma = gamma
        self._sync_rate = sync_rate

        self._total_reward = 0

        self._global_current_step = 0
        self._global_current_epoch = 0

        self._summary_writer: SummaryWriter = summary_writer
        self._model_repository: ModelRepository = model_repository

    def fit(self):
        # let's play some episodes
        for episode in range(self._n_episodes):
            # let's play an episode
            # before we start, let's get the agent in it's environment
            agent = self._agent_factory.get_agent()
            episode = Episode(agent=agent,
                              episode_number=episode,
                              global_current_step=self._global_current_step,
                              data_repository=self._data_repository,
                              epsilon_generator=self._epsilon_generator,
                              model=self._model,
                              target_model=self._target_model,
                              torch_device=self._torch_device,
                              optimizer=self._optimizer,
                              summary_writer=self._summary_writer,
                              model_repository=self._model_repository,
                              gamma=self._gamma,
                              sync_rate=self._sync_rate)
            episode.run_episode()
