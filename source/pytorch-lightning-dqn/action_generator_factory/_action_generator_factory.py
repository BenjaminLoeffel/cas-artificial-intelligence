from abc import ABC, abstractmethod

import gym
import numpy as np
import torch
from torch import nn

from action_generator import IActionGenerator, RandomActionGenerator, ModelBasedActionGenerator


class IActionGeneratorFactory(ABC):

    @abstractmethod
    def get_action_generator(self, epsilon: float) -> IActionGenerator:
        """Returns IActionGenerator based on epsilon"""


class RandomAndModelBasedActionGeneratorFactory(IActionGeneratorFactory):
    _environment: gym.Env
    _model: nn.Module
    _torch_device: torch.device

    def __init__(self, environment: gym.Env, model: nn.Module, torch_device: torch.device):
        self._environment = environment
        self._model = model
        self._torch_device = torch_device

    def get_action_generator(self, epsilon: float) -> IActionGenerator:
        if np.random.random() < epsilon:
            return RandomActionGenerator(environment=self._environment)

        else:
            return ModelBasedActionGenerator(torch_device=self._torch_device,
                                             model=self._model)
