from abc import ABCMeta, abstractmethod


class IEpsilonGenerator(metaclass=ABCMeta):
    @abstractmethod
    def get_epsilon(self, step):
        """Returns epsilon for step."""


class EpsilonGenerator(IEpsilonGenerator):
    _epsilon_start: float
    _epsilon_stop: float
    _epsilon_step_stop_decay: int
    _current_step: int

    def __init__(self, epsilon_start: float, epsilon_stop: float, epsilon_step_stop_decay: int):
        """
        :param epsilon_start:
        :param epsilon_stop:
        :param epsilon_step_stop_decay:
        """
        self._epsilon_start = epsilon_start
        self._epsilon_stop = epsilon_stop
        self._epsilon_step_stop_decay = epsilon_step_stop_decay

    def get_epsilon(self, step: int) -> float:
        """Returns epsilon for step."""
        epsilon = max(self._epsilon_stop, self._epsilon_start - step + 1 / self._epsilon_step_stop_decay)
        return epsilon
