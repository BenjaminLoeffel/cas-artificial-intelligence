from abc import ABC, abstractmethod

from torch import nn

# TODO: cross check with model_repository
from enviroment_factory import IEnvironmentFactory
from torch_model import TorchModel


class IModelFactory(ABC):

    @abstractmethod
    def get_model(self) -> nn.Module:
        """Returns nn.Module."""


class ModelFactory(IModelFactory):
    _environment_factory: IEnvironmentFactory

    def __init__(self, environment_factory: IEnvironmentFactory):
        self._environment_factory = environment_factory

    def get_model(self) -> nn.Module:
        environment = self._environment_factory.get_environment()
        obs_size = environment.observation_space.shape[0]
        n_actions = environment.action_space.n
        model = TorchModel(obs_size, n_actions)
        return model
