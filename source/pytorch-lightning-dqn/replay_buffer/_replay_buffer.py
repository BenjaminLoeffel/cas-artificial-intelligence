from abc import abstractmethod, ABCMeta
from collections import deque
from typing import Tuple, List

import numpy as np

from experience import Experience


class IReplayBuffer(metaclass=ABCMeta):
    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def append(self, experience: Experience) -> None:
        """
        Add experience to the buffer.
        """

    @abstractmethod
    def extend(self, experiences: List[Experience]) -> None:
        """
        Extend buffer with experience
        """

    @abstractmethod
    def sample(self, batch_size: int) -> Tuple:
        """
        Pick random experience from internal buffer
        :param batch_size: number experiences to take
        :return:
        """


class ReplayBuffer(IReplayBuffer):
    _buffer: deque

    def __init__(self, length: int):
        self._buffer = deque(maxlen=length)

    def __len__(self) -> int:
        return len(self._buffer)

    def append(self, experience: Experience) -> None:
        """
        Add experience to the buffer.
        """
        self._buffer.append(experience)

    def extend(self, experiences: List[Experience]) -> None:
        """
        Extend buffer with experience
        """
        self._buffer.extend(experiences)

    def sample(self, batch_size: int) -> Tuple:
        """
        Pick random experience from internal buffer
        :param batch_size: number experiences to take
        :return:
        """
        # TODO: annotate Tuple
        indices = np.random.choice(len(self._buffer), batch_size, replace=False)
        states, actions, rewards, dones, next_states = zip(*(self._buffer[idx] for idx in indices))
        return (np.array(states),
                np.array(actions),
                np.array(rewards, dtype=np.float32),
                np.array(dones, dtype=np.bool),
                np.array(next_states))
