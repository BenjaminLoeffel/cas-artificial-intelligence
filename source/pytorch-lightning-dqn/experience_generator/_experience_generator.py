import logging
from abc import ABC, abstractmethod
from typing import List

from action_generator import RandomActionGenerator
from agent_factory import IAgentFactory
from experience import Experience

_logger = logging.getLogger(__file__)


class IExperienceGenerator(ABC):

    @abstractmethod
    def generate_experience(self, number: int) -> List[Experience]:
        """Generatates number occurences of Experience."""


class RandomExperienceGenerator(IExperienceGenerator):
    _agent_factory: IAgentFactory

    def __init__(self, agent_factory: IAgentFactory):
        self._agent_factory = agent_factory

    def generate_experience(self, number: int) -> List[Experience]:
        """
        Carries out several random steps through the environment to generate experience.
        """
        experiences: List[Experience] = []

        while len(experiences) < number:
            agent = self._agent_factory.get_agent()
            _logger.debug("new agent was born")
            action_generator = RandomActionGenerator(environment=agent.environment)
            done = False
            while not done:
                action = action_generator.get_next_action()
                reward, done, experience = agent.play_action(action=action)
                experiences.append(experience)
                if len(experiences) >= number:
                    break

        return experiences
