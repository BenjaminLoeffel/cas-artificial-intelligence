from abc import ABC, abstractmethod
from typing import Any


class IModel(ABC):

    @abstractmethod
    def forward(self, x: Any) -> Any:
        """Implements forward pass."""
