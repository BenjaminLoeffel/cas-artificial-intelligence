from abc import ABC, abstractmethod
from typing import Any

import gym
import numpy as np
import torch
from torch import nn


class IActionGenerator(ABC):

    @abstractmethod
    def get_next_action(self) -> int:
        """Returns next action."""


class RandomActionGenerator(IActionGenerator):
    _environment: gym.Env

    def __init__(self, environment: gym.Env):
        self._environment = environment

    def get_next_action(self) -> int:
        action = self._environment.action_space.sample()
        return action


class ModelBasedActionGenerator(IActionGenerator):
    _torch_device: torch.device
    _model: nn.Module
    _state: Any

    def __init__(self, torch_device: torch.device, model: nn.Module, state: Any):
        self._torch_device = torch_device
        self._model = model
        self._state = state

    def get_next_action(self) -> int:
        state = torch.tensor(np.array([self._state]))

        if self._torch_device.type != "cpu":
            state = state.cuda(self._torch_device)

        q_values = self._model(state)
        # get action with highest q value
        _, action = torch.max(q_values, dim=1)
        action = int(action.item())
        return action
