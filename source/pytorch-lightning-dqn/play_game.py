import logging
import time

import gym
import matplotlib.pyplot as plt
import torch
import torch.nn
from IPython import display

from agent import Agent
from config import current_config
from logging_util.builder import LoggingBuilder
from model import Model
from model_repository import ModelRepository

plt.ion()

_logger = logging.getLogger()
builder = LoggingBuilder(_logger)
builder.add_default_loggers(__file__)


def main():
    try:
        _logger.info("main started")

        _logger.info(f"cuda available: {torch.cuda.is_available()}")

        torch_device = torch.device("cpu")

        env = gym.make("CartPole-v0")

        agent = Agent(env)

        experiment_name = "current"

        model_repository = ModelRepository(basepath=current_config.models_path.joinpath(experiment_name))

        obs_size = env.observation_space.shape[0]
        n_actions = env.action_space.n

        model = Model(obs_size, n_actions)

        model_repository.load_model(model, state_dict_name="96_1250.ckpt")

        agent.play_action()

        env.reset()
        img = plt.imshow(env.render(mode='rgb_array'))
        for t in range(1000):
            # step through environment with agent
            action, _, _ = agent.play_action()
            # action = env.action_space.sample()
            img.set_data(env.render(mode='rgb_array'))
            plt.axis('off')
            display.display(plt.gcf())
            display.clear_output(wait=True)
            state, reward, done, _ = env.step(int(action))
            if done:
                print('Score: ', t + 1)
                break
            time.sleep(1)

        env.close()

        plt.show()


    except Exception as e:
        _logger.exception(f"exception occurred {e}", exc_info=True)

    finally:
        _logger.info("main finished")


if __name__ == "__main__":
    main()
