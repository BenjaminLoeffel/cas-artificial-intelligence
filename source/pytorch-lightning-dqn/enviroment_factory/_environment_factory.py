from abc import ABC, abstractmethod

import gym


class IEnvironmentFactory(ABC):

    @abstractmethod
    def get_environment(self) -> gym.Env:
        """Returns environment."""


class EnvironmentFactory(IEnvironmentFactory):
    _environment_name: str

    def __init__(self, environment_name: str = "CartPole-v0"):
        self._environment_name = environment_name

    def get_environment(self) -> gym.Env:
        environment = gym.make(self._environment_name)
        return environment
