from abc import ABC, abstractmethod

from torch.utils.data import DataLoader

from dataset import Dataset
from replay_buffer import IReplayBuffer


class IDataRepository(ABC):

    @abstractmethod
    def get_dataloader(self) -> DataLoader:
        """Returns DataLoader."""

    @abstractmethod
    def get_replay_buffer(self) -> IReplayBuffer:
        """Returns IReplayBuffer."""


class DataRepository(IDataRepository):
    _replay_buffer: IReplayBuffer
    _episode_length: int
    _batch_size: int
    _num_cpus: int

    def __init__(self, replay_buffer: IReplayBuffer, episode_length: int, batch_size: int, num_cpus: int = 1):
        self._replay_buffer = replay_buffer
        self._episode_length = episode_length
        self._batch_size = batch_size
        self._num_cpus = num_cpus

    def get_dataloader(self) -> DataLoader:
        dataset = Dataset(self._replay_buffer, self._episode_length)
        dataloader = DataLoader(dataset=dataset,
                                batch_size=self._batch_size,
                                num_workers=int(self._num_cpus))
        return dataloader

    def get_replay_buffer(self) -> IReplayBuffer:
        return self._replay_buffer
