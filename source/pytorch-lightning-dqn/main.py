import datetime
import logging

import torch
import torch.nn
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter

from agent_factory import AgentFactory
from config import current_config
from data_repository import DataRepository
from enviroment_factory import EnvironmentFactory
from epsilon_generator import EpsilonGenerator
from experience_generator import RandomExperienceGenerator
from logging_util.builder import LoggingBuilder
from model_factory import ModelFactory
from model_repository import ModelRepository
from replay_buffer import ReplayBuffer
from trainer import Trainer

_logger = logging.getLogger()
builder = LoggingBuilder(_logger)
builder.add_default_loggers(__file__)


def main():
    try:
        _logger.info("main started")

        _logger.info(f"cuda available: {torch.cuda.is_available()}")

        experiment_name = f"experiment_{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}"
        experiment_name = "current"

        torch_device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        torch_device = torch.device("cpu")

        environment_factory = EnvironmentFactory()

        agent_factory = AgentFactory(environment_factory=environment_factory)

        experience_generator = RandomExperienceGenerator(agent_factory=agent_factory)
        initial_experience = experience_generator.generate_experience(number=1000)

        replay_buffer = ReplayBuffer(length=1000)
        replay_buffer.extend(initial_experience)

        model_factory = ModelFactory(environment_factory=environment_factory)
        model = model_factory.get_model()
        target_model = model_factory.get_model()

        data_repository = DataRepository(replay_buffer=replay_buffer, episode_length=200, batch_size=16)

        learning_rate = 1e-2
        optimizer = Adam(model.parameters(), lr=learning_rate)

        summary_writer = SummaryWriter(log_dir=str(current_config.tensorboard_path.joinpath(experiment_name)))

        model_repository = ModelRepository(basepath=current_config.models_path.joinpath(experiment_name))

        epsilon_generator = EpsilonGenerator(epsilon_start=1.0, epsilon_stop=0.01, epsilon_step_stop_decay=1000)

        trainer = Trainer(agent_factory=agent_factory,
                          data_repository=data_repository,
                          epsilon_generator=epsilon_generator,
                          model=model,
                          target_model=target_model,
                          torch_device=torch_device,
                          optimizer=optimizer,
                          summary_writer=summary_writer,
                          n_episodes=1000,
                          model_repository=model_repository)
        trainer.fit()

    except Exception as e:
        _logger.exception(f"exception occurred {e}", exc_info=True)

    finally:
        _logger.info("main finished")


if __name__ == "__main__":
    main()
