from abc import ABC, abstractmethod

from agent import IAgent, Agent
from enviroment_factory import IEnvironmentFactory


class IAgentFactory(ABC):

    @abstractmethod
    def get_agent(self) -> IAgent:
        """Returns IAgent."""


class AgentFactory(IAgentFactory):
    _environment_factory: IEnvironmentFactory

    def __init__(self, environment_factory: IEnvironmentFactory):
        self._environment_factory = environment_factory

    def get_agent(self) -> IAgent:
        environment = self._environment_factory.get_environment()
        initial_state = environment.reset()
        agent = Agent(environment=environment, initial_state=initial_state)
        return agent
